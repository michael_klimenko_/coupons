# Coupons : Phase I
## This repository includes Database Back-end for Coupon project. ###

#### What is this repository for? ####

Coupon management system is intended to be a proof of concept for themes learned at Java Course.
It is the first part of Coupon project, and it introduces "Database layer and Business Logic" 

###Phase I includes:

* Java beans
* Interfaces
* Singletones
* DAO classes to manage DB entities
* Facades wrappers for DAO classes
* Exception management
* Threads
* JUnit Tests



The following repos are part of this project:

* REST    [Coupon Project Phase II](https://bitbucket.org/michael_klimenko_/couponrest)
* UI      [Coupon Project Phase I](https://bitbucket.org/michael_klimenko_/couponsystem)

===================


### How do I get set up? ###

# Dependencies

* Development
-[JDK 1.8](https://java.com)

* Build and dependency management
-[Apache Maven](https://maven.apache.org/)

* Test Framework 
-[Junit Surefire plugin](http://maven.apache.org/surefire/maven-surefire-plugin/examples/junit.html)

* Configuration
-[Hocon](https://github.com/typesafehub/config)

* Database
[JavaDB](https://netbeans.org/kb/docs/ide/java-db.html)
> I used [derbyclient-10.11.1.1](https://mvnrepository.com/artifact/org.apache.derby/derbyclient/10.11.1.1) for database interactions.

# Configuration
all settings are in `application.conf` file

# Database configuration
run `Tester.java` to initialize DB with mock-up values from `InitDB.java` class.

# Deployment instructions
add as dependency in Phase II CouponREST.

# Running Tests
running `build` will fire tests.
>Note: DB populated with slightly different values for JUnit tests.
>in order to get mock-up values for testing REST or UI (Phase II), need to run `Tester.java`.


### Who do I talk to? ###

* michael.klimenko@gmail.com