package com.michael.coupon.web.utils.cgrates;

import com.michael.coupons.exceptions.ApplicationException;
import com.michael.coupons.utils.Exceptions;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Account {

    private String ID;
    private boolean AllowNegative;
    private boolean Disabled;
    // private JSONObject BalanceMap;
    private Map<String, ArrayList<Balance>> BalanceMap;

    public Account() {
	this.ID = "";
	this.AllowNegative = false;
	this.Disabled = false;
	// this.BalanceMap = new JSONObject();
	this.BalanceMap = new HashMap<>();
    }

    public Account(JSONObject object) {
	if (!object.isNull("ID"))
	    this.ID = object.optString("ID", null);
	if (!object.isNull("AllowNegative"))
	    this.AllowNegative = object.optBoolean("AllowNegative");
	if (!object.isNull("Disabled"))
	    this.Disabled = object.optBoolean("Disabled");
	if (!object.isNull("BalanceMap")) {

	    this.BalanceMap = new HashMap<>();
	    
	    JSONArray arrayResult = null;
	    ArrayList<Balance> listObjects = null;
	    try {
		JSONObject tmpBalances = object.getJSONObject("BalanceMap");
	
		Iterator<?> keys = tmpBalances.keys();

		while (keys.hasNext()) {
		    String key = (String) keys.next();
		    if (tmpBalances.get(key) instanceof JSONArray) {
			arrayResult = tmpBalances.getJSONArray(key);
			if (arrayResult != null) {
			    listObjects = new ArrayList<>();
			    int lenght = arrayResult.length();
			    for (int i = 0; i < lenght; i++) {
				JSONObject obj = arrayResult.getJSONObject(i);
				Balance balance = new Balance(obj);
				listObjects.add(balance);
			    }
			}
		    }
		    this.BalanceMap.put(key,listObjects);
		}

	    } catch (JSONException ex) {
		throw new ApplicationException(Exceptions.ACCOUNT, ex);
	    }
	}

    }

    public JSONObject createJsonRpc(String _id, String _method) throws JSONException {

	// Create message and body
	// The remote method to call
	String method = _method;

	// The required named parameters to pass
	Map<String, Object> params = new HashMap<>();

	params.put("ID", getID());
	params.put("AllowNegative", isAllowNegative());
	params.put("Disabled", isDisabled());
	params.put("BalanceMap", getBalanceMap());

	// The mandatory request ID
	String id = _id;

	Collection<Map<String, Object>> paramArr = new ArrayList<>();
	paramArr.add(params);
	// Create a new JSON-RPC 1.0 request
	JSONObject reqOut = new JSONObject();
	reqOut.put("id", id);
	reqOut.put("method", method);
	reqOut.put("params", paramArr);

	return reqOut;

    }

    public String getID() {
	return ID;
    }

    public void setID(String iD) {
	ID = iD;
    }

    public boolean isAllowNegative() {
	return AllowNegative;
    }

    public void setAllowNegative(boolean allowNegative) {
	AllowNegative = allowNegative;
    }

    public boolean isDisabled() {
	return Disabled;
    }

    public void setDisabled(boolean disabled) {
	Disabled = disabled;
    }

    public Map<String, ArrayList<Balance>> getBalanceMap() {
	return BalanceMap;
    }

    public void setBalanceMap(Map<String, ArrayList<Balance>> balanceMap) {
	BalanceMap = balanceMap;
    }

}
