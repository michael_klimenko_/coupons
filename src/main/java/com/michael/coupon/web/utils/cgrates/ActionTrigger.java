/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.michael.coupon.web.utils.cgrates;

import java.time.Duration;
import java.util.Date;

/**
type ActionTrigger struct {
	ID            string // original csv tag
	UniqueID      string // individual id
	ThresholdType string //*min_event_counter, *max_event_counter, *min_balance_counter, *max_balance_counter, *min_balance, *max_balance, *balance_expired
	// stats: *min_asr, *max_asr, *min_acd, *max_acd, *min_tcd, *max_tcd, *min_acc, *max_acc, *min_tcc, *max_tcc, *min_ddc, *max_ddc
	ThresholdValue float64
	Recurrent      bool          // reset excuted flag each run
	MinSleep       time.Duration // Minimum duration between two executions in case of recurrent triggers
	ExpirationDate time.Time
	ActivationDate time.Time
	//BalanceType       string // *monetary/*voice etc
	Balance           *BalanceFilter
	Weight            float64
	ActionsID         string
	MinQueuedItems    int // Trigger actions only if this number is hit (stats only)
	Executed          bool
	LastExecutionTime time.Time
}
 */
class ActionTrigger {
private String ID; //            string // original csv tag
private	String UniqueID; //      string // individual id
private	String ThresholdType; // string //*min_event_counter, *max_event_counter, *min_balance_counter, *max_balance_counter, *min_balance, *max_balance, *balance_expired
// stats: *min_asr, *max_asr, *min_acd, *max_acd, *min_tcd, *max_tcd, *min_acc, *max_acc, *min_tcc, *max_tcc, *min_ddc, *max_ddc
private	Double ThresholdValue;// float64
private	Boolean Recurrent; //      bool          // reset excuted flag each run
private	Duration MinSleep; //       time.Duration // Minimum duration between two executions in case of recurrent triggers
private	Date ExpirationDate; // time.Time
private	Date ActivationDate; // time.Time
//BalanceType       string // *monetary/*voice etc
private	BalanceFilter Balance; //           *BalanceFilter
private	Double Weight; //            float64
private	String ActionsID; //         string
private	Integer MinQueuedItems; //    int // Trigger actions only if this number is hit (stats only)
private	Boolean Executed; //          bool
private	Date LastExecutionTime; // time.Time 

        }
