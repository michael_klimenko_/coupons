package com.michael.coupon.web.utils.cgrates;

import com.michael.coupons.utils.ApplicationUtilities;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/**
type AttrGetAccount struct {
	Tenant  string
	Account string
}
 */
public class AttrGetAccount {
    
    private String Tenant;
    private String Account;

    public AttrGetAccount() {
        this.Tenant = ApplicationUtilities.CONFIG.getString("app.cgrates.default.tenant");
    }

    public AttrGetAccount(String Account) {
        this.Tenant = ApplicationUtilities.CONFIG.getString("app.cgrates.default.tenant");
        this.Account = Account;
    }
    
    public AttrGetAccount(String Tenant, String Account) {
        this.Tenant = Tenant;
        this.Account = Account;
    }
    
    public String getTenant() {
        return Tenant;
    }

    public void setTenant(String Tenant) {
        this.Tenant = Tenant;
    }

    public String getAccount() {
        return Account;
    }

    public void setAccount(String Account) {
        this.Account = Account;
    }
    
    public JSONObject createJsonRpc(String _id, String _method) throws JSONException {

	// Create message and body
	// The remote method to call
	String method = _method;

	// The required named parameters to pass
	Map<String, Object> params = new HashMap<>();

	params.put("Tenant", getTenant());
	params.put("Account", getAccount());

	// The mandatory request ID
	String id = _id;

	Collection<Map<String, Object>> paramArr = new ArrayList<>();
	paramArr.add(params);
	// Create a new JSON-RPC 1.0 request
	JSONObject reqOut = new JSONObject();
	reqOut.put("id", id);
	reqOut.put("method", method);
	reqOut.put("params", paramArr);

	return reqOut;

    }
    
}
