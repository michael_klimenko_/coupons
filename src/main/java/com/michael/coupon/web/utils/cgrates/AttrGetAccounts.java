package com.michael.coupon.web.utils.cgrates;

import com.michael.coupons.utils.ApplicationUtilities;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/**
type AttrGetAccounts struct {
	Tenant     string
	AccountIds []string
	Offset     int // Set the item offset
	Limit      int // Limit number of items retrieved
}
}
 */
public class AttrGetAccounts {
    
    private String Tenant;
    private String[] AccountIds;
    private Integer Offset;
    private Integer Limit;
    

    public AttrGetAccounts() {
        this.Tenant = ApplicationUtilities.CONFIG.getString("app.cgrates.default.tenant");
        this.Limit =  ApplicationUtilities.CONFIG.getInt("app.cgrates.default.limit");
    }

    public AttrGetAccounts(Integer Limit) {
        this.Tenant = ApplicationUtilities.CONFIG.getString("app.cgrates.default.tenant");
        this.Limit = Limit;
    }

    public AttrGetAccounts(String Tenant, Integer Limit) {
        this.Tenant = Tenant;
        this.Limit = Limit;
    }

    
    
    public AttrGetAccounts(String Tenant, Integer Offset, Integer Limit) {
        this.Tenant = Tenant;
        this.Offset = Offset;
        this.Limit = Limit;
    }

    
    
    public AttrGetAccounts(String Tenant, String[] AccountIds, Integer Offset, Integer Limit) {
        this.Tenant = Tenant;
        this.AccountIds = AccountIds;
        this.Offset = Offset;
        this.Limit = Limit;
    }
    
    
    public String getTenant() {
        return Tenant;
    }

    public void setTenant(String Tenant) {
        this.Tenant = Tenant;
    }

    public String[] getAccountIds() {
        return AccountIds;
    }

    public void setAccountIds(String[] AccountIds) {
        this.AccountIds = AccountIds;
    }

    public Integer getOffset() {
        return Offset;
    }

    public void setOffset(Integer Offset) {
        this.Offset = Offset;
    }

    public Integer getLimit() {
        return Limit;
    }

    public void setLimit(Integer Limit) {
        this.Limit = Limit;
    }
    
    public JSONObject createJsonRpc(String _id, String _method) throws JSONException {

	// Create message and body
	// The remote method to call
	String method = _method;

	// The required named parameters to pass
	Map<String, Object> params = new HashMap<>();

        params.put("Tenant", getTenant());
        
        if (getAccountIds() != null) {
            params.put("Tenant", getTenant());
        }
        if (getOffset()!= null) {
            params.put("Offset", getOffset());
        }
         if (getLimit()!= null) {
            params.put("Limit", getLimit());
        }
	


	// The mandatory request ID
	String id = _id;

	Collection<Map<String, Object>> paramArr = new ArrayList<>();
	paramArr.add(params);
	// Create a new JSON-RPC 1.0 request
	JSONObject reqOut = new JSONObject();
	reqOut.put("id", id);
	reqOut.put("method", method);
	reqOut.put("params", paramArr);

	return reqOut;

    }

}
