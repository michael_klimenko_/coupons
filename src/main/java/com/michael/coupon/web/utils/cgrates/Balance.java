package com.michael.coupon.web.utils.cgrates;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;

import org.json.JSONObject;

public class Balance {

    private String Uuid;
    private String ID;
    private Double Value;
    private String ExpirationDate;
    private boolean Disabled;

    public Balance() {
        this.Uuid = "";
        this.ID = "";
        this.Value = 0D;
        this.ExpirationDate = "0001-01-01T00:00:00Z";
        this.Disabled = false;
    }

    public Balance(JSONObject object) {
        try {
            this.Uuid = object.get("Uuid").toString();
            this.ID = object.get("ID").toString();
            this.Value = object.getDouble("Value");
            this.ExpirationDate = object.get("ExpirationDate").toString();
            this.Disabled = object.getBoolean(("Disabled"));
            
        } catch (JSONException ex) {
            Logger.getLogger(Balance.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public JSONObject createJsonRpc(String _id, String _method) {

        // Create message and body
        // The remote method to call
        String method = _method;

        // The required named parameters to pass
        Map<String, Object> params = new HashMap<>();

        params.put("Uuid", getUuid());
        params.put("ID", getID());
        params.put("Value", getValue());
        params.put("ExpirationDate", getExpirationDate());
        params.put("Disabled", isDisabled());

        // The mandatory request ID
        String id = _id;

        Collection<Map<String, Object>> paramArr = new ArrayList<>();
        paramArr.add(params);
        // Create a new JSON-RPC 1.0 request
        JSONObject reqOut = new JSONObject();
        try {
            reqOut.put("id", id);
            reqOut.put("method", method);
            reqOut.put("params", paramArr);

        } catch (JSONException ex) {
            Logger.getLogger(Balance.class.getName()).log(Level.SEVERE, null, ex);
        }

        return reqOut;

    }

    public String getUuid() {
        return Uuid;
    }

    public void setUuid(String uuid) {
        Uuid = uuid;
    }

    public String getID() {
        return ID;
    }

    public void setID(String iD) {
        ID = iD;
    }

    public Double getValue() {
        return Value;
    }

    public void setValue(Double value) {
        Value = value;
    }

    public String getExpirationDate() {
        return ExpirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        ExpirationDate = expirationDate;
    }

    public boolean isDisabled() {
        return Disabled;
    }

    public void setDisabled(boolean disabled) {
        Disabled = disabled;
    }

}
