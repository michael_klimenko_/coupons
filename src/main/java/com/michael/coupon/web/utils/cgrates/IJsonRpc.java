package com.michael.coupon.web.utils.cgrates;

import org.json.JSONObject;

public interface IJsonRpc {
 //Create JSON RPC method stub
    JSONObject createJsonRpc(String _id, String _method, java.util.Map<String, Object> params);
    JSONObject createJsonRpc(String requestId, String _method);
}
