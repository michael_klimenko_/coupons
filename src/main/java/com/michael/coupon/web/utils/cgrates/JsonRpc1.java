package com.michael.coupon.web.utils.cgrates;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;

import org.json.JSONObject;
import org.json.JSONString;

public abstract class JsonRpc1 implements IJsonRpc {

    @Override
    public JSONObject createJsonRpc(String _id, String _method, Map<String, Object> params) {
	// The mandatory request ID
	String id = _id;

	// Create message and body of The remote method to call
	String method = _method;

	// The required named parameters to pass
	Collection<Map<String, Object>> paramArr = new ArrayList<>();
	paramArr.add(params);

	// Create a new JSON-RPC 1.0 request
	JSONObject reqOut = new JSONObject();
        try {
            reqOut.put("id", id);
            reqOut.put("method", method);
            reqOut.put("params", paramArr);

        } catch (JSONException ex) {
            Logger.getLogger(JsonRpc1.class.getName()).log(Level.SEVERE, null, ex);
        }
	
	return reqOut;
    }

  

}
