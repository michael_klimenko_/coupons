/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.michael.coupon.web.utils.cgrates;

import java.time.Month;
import java.time.MonthDay;
import java.time.Year;
import java.time.DayOfWeek;

/**
type RITiming struct {
	Years              utils.Years
	Months             utils.Months
	MonthDays          utils.MonthDays
	WeekDays           utils.WeekDays
	StartTime, EndTime string // ##:##:## format
	cronString         string
	tag                string // loading validation only
}
 */
class RITiming {
    private Year[] Years; //              utils.Years
    private Month[] Months; //             utils.Months
    private MonthDay[] MonthDays; //          utils.MonthDays
    private DayOfWeek[] WeekDays; //           utils.WeekDays
    private String StartTime, EndTime; // string // ##:##:## format
    private String cronString; //         string
    private String tag; //               string // loading validation only
}
