/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.michael.coupon.web.utils.cgrates;

/**
// Amount of a trafic of a certain type
type UnitCounter struct {
	CounterType string         // *event or *balance
	Counters    CounterFilters // first balance is the general one (no destination)
}
* 
* type CounterFilter struct {
	Value  float64
	Filter *BalanceFilter
}
 */
class UnitCounter {
    private String CounterType; 
    private CounterFilter[] Counters;
}


class CounterFilter {
    private Double Value;
    private BalanceFilter Filter;
}

