//package com.michael.coupon.web.utils.cgrates;
//
//import java.util.Map;
//
///**
// * type Account struct { ID string BalanceMap map[string]Balances UnitCounters UnitCounters ActionTriggers ActionTriggers AllowNegative bool Disabled
// * bool executingTriggers bool
// *
// * type UnitCounters map[string][]*UnitCounter
// */
//public class Account {
//
//    private String ID;
//    private Map<String, Balance> BalanceMap;
//    private Map<String, UnitCounter[]> UnitCounters;
//    private ActionTrigger[] ActionTriggers;
//    private Boolean AllowNegative;
//    private Boolean Disabled;
//    private Boolean executingTriggers;
//
//    public Account() {
//    }
//
//    public String getID() {
//        return ID;
//    }
//
//    public void setID(String ID) {
//        this.ID = ID;
//    }
//
//    public Map<String, Balance> getBalanceMap() {
//        return BalanceMap;
//    }
//
//    public void setBalanceMap(Map<String, Balance> BalanceMap) {
//        this.BalanceMap = BalanceMap;
//    }
//
//    public Map<String, UnitCounter[]> getUnitCounters() {
//        return UnitCounters;
//    }
//
//    public void setUnitCounters(Map<String, UnitCounter[]> UnitCounters) {
//        this.UnitCounters = UnitCounters;
//    }
//
//    public ActionTrigger[] getActionTriggers() {
//        return ActionTriggers;
//    }
//
//    public void setActionTriggers(ActionTrigger[] ActionTriggers) {
//        this.ActionTriggers = ActionTriggers;
//    }
//
//    public Boolean getAllowNegative() {
//        return AllowNegative;
//    }
//
//    public void setAllowNegative(Boolean AllowNegative) {
//        this.AllowNegative = AllowNegative;
//    }
//
//    public Boolean getDisabled() {
//        return Disabled;
//    }
//
//    public void setDisabled(Boolean Disabled) {
//        this.Disabled = Disabled;
//    }
//
//    public Boolean getExecutingTriggers() {
//        return executingTriggers;
//    }
//
//    public void setExecutingTriggers(Boolean executingTriggers) {
//        this.executingTriggers = executingTriggers;
//    }
//
//    @Override
//    public String toString() {
//        return "Account{" + "ID=" + ID + ", BalanceMap=" + BalanceMap + ", UnitCounters=" + UnitCounters + ", ActionTriggers=" + ActionTriggers + ", AllowNegative=" + AllowNegative + ", Disabled=" + Disabled + ", executingTriggers=" + executingTriggers + '}';
//    }
//
//    
//}
