//package com.michael.coupon.web.utils.cgrates;
//
//import java.util.Date;
//import java.util.Map;
//
///**
//// Can hold different units as seconds or monetary
//type Balance struct {
//	Uuid           string //system wide unique
//	ID             string // account wide unique
//	Value          float64
//	Directions     utils.StringMap
//	ExpirationDate time.Time
//	Weight         float64
//	DestinationIDs utils.StringMap
//	RatingSubject  string
//	Categories     utils.StringMap
//	SharedGroups   utils.StringMap
//	Timings        []*RITiming
//	TimingIDs      utils.StringMap
//	Disabled       bool
//	Factor         ValueFactor
//	Blocker        bool
//	precision      int
//	account        *Account // used to store ub reference for shared balances
//	dirty          bool
//}
// */
//public class Balance {
//    private String Uuid;; //           string //system wide unique
//    private String ID; //             string // account wide unique
//    private Double Value; //          float64
//    private Map<String,Boolean> Directions; //    utils.StringMap
//    private Date ExpirationDate; // time.Time
//    private Double Weight; //         float64
//    private Map<String,Boolean> DestinationIDs; // utils.StringMap
//    private String RatingSubject; //  string
//    private Map<String,Boolean> Categories; //    utils.StringMap
//    private Map<String,Boolean> SharedGroups; //   utils.StringMap
//    private RITiming Timings; //        []*RITiming
//    private Map<String,Boolean> TimingIDs; //      utils.StringMap
//    private Boolean	Disabled; //      bool
//    private Map<String,Double> Factor; //         ValueFactor
//    private Boolean Blocker; //        bool
//    private Integer precision; //      int
//    private Account	account;//        *Account // used to store ub reference for shared balances
//    private Boolean	dirty;
//}
