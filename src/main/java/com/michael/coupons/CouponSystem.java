package com.michael.coupons;

import com.michael.coupons.exceptions.ApplicationException;
import com.michael.coupons.facades.AdminFacade;
import com.michael.coupons.facades.CompanyFacade;
import com.michael.coupons.facades.CustomerFacade;
import com.michael.coupons.facades.ICouponClientFacade;
import static com.michael.coupons.utils.ApplicationUtilities.log;
import com.michael.coupons.utils.ClientType;
import com.michael.coupons.utils.DailyCouponExpiration;
import static com.michael.coupons.utils.ApplicationUtilities.log;

/**
 * CouponSystem is the main class that provides clientFacade class for interaction with the system by login.
 * it also initialize coupon expiration mechanism that runs every x time and removes expired coupons.
 * @author michael
 */
public class CouponSystem {
    
    // =============== PROPERTIES ===================
    
    // Facade class used for interaction DAO classes
    // Login function will instantiate the suitable Facade.
    private ICouponClientFacade clientFacade;
    
    //expiration thread
    private final DailyCouponExpiration cleanExpiredCoupons = new DailyCouponExpiration();
    
    private static final CouponSystem INSTANCE = new CouponSystem();
    
    private CouponSystem() {
        
       log("CouponSystem() started"); 
       
       log("java.classpath="+System.getProperty("java.classpath"));
       
       log("cleanExpiredCoupons() starting");
       //start expiration thread
       cleanExpiredCoupons.start();
        
    }
    
    public static CouponSystem getInstance() {
        return INSTANCE;
    } 
    
    /**
     * Method used to return suitable facade by provided client type.
     * In case forceCheck is true, provided credentials will be checked 
     * against the database (except for admin, which is hardcoded)
     * @param username
     * @param password
     * @param clientType - type of facade to return
     * @param forceCheck - force check credentials
     * @return
     * @throws ApplicationException
     */
    public ICouponClientFacade login(String username, String password, ClientType clientType,boolean forceCheck) throws ApplicationException {
    
        switch (clientType) {
            case ADMIN          : clientFacade =  AdminFacade.getInstance();     break;
            case COMPANY        : clientFacade =  CompanyFacade.getInstance();   break;
            case CUSTOMER       : clientFacade =  CustomerFacade.getInstance();  break;
            default             : clientFacade =  CustomerFacade.getInstance();  break;
        }
    if (forceCheck) {
        if (clientFacade.login(username, password, clientType)) {
            return clientFacade;
        } else {
            return null;
        }
    }else {
        return clientFacade;
    }
    }
    
    public void shutdown() {
    log("CouponSystem() shutdown");
    cleanExpiredCoupons.stopTask();
    log("cleanExpiredCoupons() stopped");  
    }
}
