package com.michael.coupons.beans;

import java.io.Serializable;
import java.util.Collection;
import javax.xml.bind.annotation.XmlTransient;

/**
 * This class represents the Company model. This model class can be used throughout all
 * layers, the data layer, the controller layer and the view layer.
 * @author michael
 */
public class Company implements Serializable {

    // =============== CONSTANTS ===================
    
    private static final long serialVersionUID = 1L;
    
    // =============== PROPERTIES ===================
    
    private Long id;
    private String comp_name;
    private String password;
    private String email;
    private Collection<Coupon> coupons;

    
    // =============== CONSTRUCTORS ===================
    
    public Company() {
    }

    public Company(Long id, String comp_name, String password, String email) {
        this.id = id;
        this.comp_name = comp_name;
        this.password = password;
        this.email = email;
        
    }
    public Company(Long id, String comp_name, String password, String email, Collection<Coupon> coupons) {
        this.id = id;
        this.comp_name = comp_name;
        this.password = password;
        this.email = email;
        this.coupons = coupons;
    }

    // =============== GETTERS & SETTERS ===================
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getComp_name() {
        return comp_name;
    }

    public void setComp_name(String comp_name) {
        this.comp_name = comp_name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Collection<Coupon> getCoupons() {
        return coupons;
    }

    public void setCoupons(Collection<Coupon> coupons) {
        this.coupons = coupons;
    }

    // =============== OVERRIDES OBJECTS ===================

    /**
     * The user ID is unique for each Company. So this should compare Company by ID only.
     * @param other The object to compare with
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object other) {
       return (other instanceof Company) && (id != null)
             ? id.equals(((Company) other).id)
             : (other == this);
    }
    
    /**
     * The user ID is unique for each Company. So Company with same ID should return same hashcode.
     * @return hashcode
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return (id != null) 
             ? (this.getClass().hashCode() + id.hashCode()) 
             : super.hashCode();
    }

    /**
     * Returns the String representation of Company model object.
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Company{" + "id=" + id + ", comp_name=" + comp_name + ", password=" + password + ", email=" + email + ", coupons=\n" + coupons + '}';
    }

    
}
