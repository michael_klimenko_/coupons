
package com.michael.coupons.beans;

import com.michael.coupons.entities.Category;
import java.io.Serializable;
import java.sql.Date;
import java.time.Instant;

/**
 * This class represents the Coupon model. This model class can be used throughout all
 * layers, the data layer, the controller layer and the view layer.
 * 
 * @author michael
 */

public class Coupon  implements Serializable {

    // =============== CONSTANTS ===================
    
    private static final long serialVersionUID = 1L;
    
    // =============== PROPERTIES ===================
    
    private Long id;
    private String title;
    private Date startDate;
    private Date endDate;
    private Integer amount;
    private Category type;
    private String message;
    private Double price;
    private String image;


    // =============== CONSTRUCTORS ===================

    public Coupon() {
    }

    /**
     * Coupon constructor with all the attributes.
     * @param id Coupon ID.
     * @param title Coupon Title.
     * @param startDate Coupon activation date.
     * @param endDate Coupone xpiration date.
     * @param amount Coupon amount in stock.
     * @param type Coupon Category type.
     * @param message Coupon promotion message
     * @param price Coupon price.
     * @param image Coupon image reference.
     */
    public Coupon(Long id, String title, Date startDate, Date endDate, Integer amount, Category type, String message, Double price, String image) {
        this.id = id;
        this.title = title;
        this.startDate = startDate;
        this.endDate = endDate;
        this.amount = amount;
        this.type = type;
        this.message = message;
        this.price = price;
        this.image = image;
    }

    // =============== GETTERS & SETTERS ===================
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public int getType() {
        return type.getValue();
    }

    public void setType(Category type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

   
    // =============== OVERRIDES OBJECTS ===================
    
    /**
     * The user ID is unique for each Coupon. So this should compare Coupon by ID only.
     * @param other The object to compare with
     * @return The object to compare with
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object other) {
        return (other instanceof Coupon) && (id != null)
             ? id.equals(((Coupon) other).id)
             : (other == this);
    }

    /**
     * The user ID is unique for each Coupon. So Coupon with same ID should return same hashcode.
     * @return hashcode 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return (id != null) 
             ? (this.getClass().hashCode() + id.hashCode()) 
             : super.hashCode();
    }
    
    /**
     * Returns the String representation of this Coupon model object.
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Coupon{" + "id=" + id + ", title=" + title + ", startDate=" + startDate + ", endDate=" + endDate + ", amount=" + amount + ", type=" + type + ", message=" + message + ", price=" + price + ", image=" + image + "}";
    }
    
    
    
}
