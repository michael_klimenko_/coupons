package com.michael.coupons.beans;

import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;

/**
 * This class represents the Customer model. This model class can be used throughout all
 * layers, the data layer, the controller layer and the view layer.
 * @author michael
 */
public class Customer implements Serializable {
    
    // =============== CONSTANTS ===================
    
    private static final long serialVersionUID = 1L;
    
    // =============== PROPERTIES ===================
    
    private Long id;
    private String cust_name;
    private String password;
    private Collection<Coupon> coupons;

    // =============== CONSTRUCTORS ===================
    
    public Customer() {
    }
    public Customer(Long id, String cust_name, String password) {
        this.id = id;
        this.cust_name = cust_name;
        this.password = password;
    }
    public Customer(Long id, String cust_name, String password, Collection<Coupon> coupons) {
        this.id = id;
        this.cust_name = cust_name;
        this.password = password;
        this.coupons = coupons;
    }

    // =============== GETTERS & SETTERS ===================
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCust_name() {
        return cust_name;
    }

    public void setCust_name(String cust_name) {
        this.cust_name = cust_name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Collection<Coupon> getCoupons() {
        return coupons;
    }

    public void setCoupons(Collection<Coupon> coupons) {
        this.coupons = coupons;
    }
        
   
    // =============== OVERRIDES OBJECTS ===================

    /**
     * The user ID is unique for each Customer. So this should compare Customer by ID only.
     * @param other The object to compare with
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object other) {
        return (other instanceof Customer) && (id != null)
             ? id.equals(((Customer) other).id)
             : (other == this);
    }
    
    /**
     * The user ID is unique for each Customer. So Customer with same ID should return same hashcode.
     * @return hashcode
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
         return (id != null) 
             ? (this.getClass().hashCode() + id.hashCode()) 
             : super.hashCode();
    }
    
    /**
     * Returns the String representation of Customer model object.
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Customer{" + "id=" + id + ", cust_name=" + cust_name + ", password=" + password + ", coupons=\n" + coupons + '}';
    }

    
    
}
