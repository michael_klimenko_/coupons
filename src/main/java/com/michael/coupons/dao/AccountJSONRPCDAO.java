package com.michael.coupons.dao;

import com.michael.coupon.web.utils.cgrates.Account;
import com.michael.coupon.web.utils.cgrates.AttrGetAccount;
import com.michael.coupon.web.utils.cgrates.AttrGetAccounts;
import com.michael.coupon.web.utils.cgrates.Balance;
import com.michael.coupons.dao.interfaces.IAccountDAO;
import com.michael.coupons.exceptions.ApplicationException;
import com.michael.coupons.utils.ApplicationUtilities;
import com.michael.coupons.utils.Exceptions;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.Charsets;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;


public class AccountJSONRPCDAO implements IAccountDAO{
    
   // ======================CONSTANTS========================
    private static final long serialVersionUID = 1L;

    private final CloseableHttpClient httpclient;
    private final HttpPost post;
    private final ResponseHandler<String> responseHandler;
    // ==============SINGLETONE======================
    private static final AccountJSONRPCDAO INSTANCE = new AccountJSONRPCDAO();

    private AccountJSONRPCDAO() {
      httpclient = HttpClients.createDefault();
      post = new HttpPost(ApplicationUtilities.CONFIG.getString("app.cgrates.webservice"));
      post.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
      
      
       // Create a custom response handler
      responseHandler = new ResponseHandler<String>() {
                @Override
                public String handleResponse(
                        final HttpResponse response) throws ClientProtocolException, IOException {
                    int status = response.getStatusLine().getStatusCode();
                    if (status >= 200 && status < 300) {
                        HttpEntity entity = response.getEntity();
                        JSONObject result = new JSONObject(entity != null ? EntityUtils.toString(entity) : null);
                        //System.out.println("response handler: " + result.toString());
                        return result.toString();
                    } else {
                        throw new ClientProtocolException("Unexpected response status: " + status);
                    }
                }

            };
    }

    public static AccountJSONRPCDAO getInstance() {
        return INSTANCE;
    }

    @Override
    public Account getAccount(AttrGetAccount attrGetAccount) throws ApplicationException{
        try {
            post.setEntity(new StringEntity(attrGetAccount.createJsonRpc(ApplicationUtilities.getId(), "ApierV2.GetAccount").toString(), Charsets.UTF_8));
            return new Account(new JSONObject(httpclient.execute(post, responseHandler)).getJSONObject("result"));
        } catch (IOException ex) {
            throw new ApplicationException(Exceptions.ACCOUNT, ex);
        }
    }
    
     @Override
    public Account[] getAccounts(AttrGetAccounts attrGetAccounts) throws ApplicationException{
        Account[] accounts = null;
        try {
            post.setEntity(new StringEntity(attrGetAccounts.createJsonRpc(ApplicationUtilities.getId(), "ApierV2.GetAccounts").toString(), Charsets.UTF_8));
            JSONArray arrayResult = new JSONObject(httpclient.execute(post, responseHandler)).getJSONArray("result");
	    if (arrayResult != null) {
			    accounts = new Account[arrayResult.length()];
			    int lenght = arrayResult.length();
			    for (int i = 0; i < lenght; i++) {
				JSONObject obj = arrayResult.getJSONObject(i);
				Account account = new Account(obj);
				accounts[i] = account;
			    }
			}
            return accounts;
        } catch (IOException ex) {
            throw new ApplicationException(Exceptions.ACCOUNT, ex);
        }
    }

}
