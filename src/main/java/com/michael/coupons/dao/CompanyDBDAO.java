package com.michael.coupons.dao;

import com.michael.coupons.dao.interfaces.ICompanyDAO;
import com.michael.coupons.beans.Company;
import com.michael.coupons.beans.Coupon;
import com.michael.coupons.database.DBConnetionPoolManagerFactory;
import com.michael.coupons.database.IDBConnectionPoolManager;
import com.michael.coupons.entities.Category;
import com.michael.coupons.exceptions.ApplicationException;
import com.michael.coupons.utils.ApplicationUtilities;
import com.michael.coupons.utils.Exceptions;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import static com.michael.coupons.utils.ApplicationUtilities.CONFIG;
import java.sql.Statement;

public class CompanyDBDAO implements ICompanyDAO {

    // ======================CONSTANTS========================
    private static final long serialVersionUID = 1L;

    private final IDBConnectionPoolManager dbConnPoolMgr;

    // ==============SINGLETONE======================
    private static final CompanyDBDAO INSTANCE = new CompanyDBDAO();

    private CompanyDBDAO() {
        this.dbConnPoolMgr = DBConnetionPoolManagerFactory.getInstance();
    }

    public static CompanyDBDAO getInstance() {
        return INSTANCE;
    }

    @Override
    public Company createCompany(Company company) throws ApplicationException {

        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {

            //Read SQL statement template from CONFIG
            String sql = ApplicationUtilities.CONFIG.getString("sql.company.create");

            //get DB connection
            conn = dbConnPoolMgr.getConnectionFromPool();
            stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            //SQL Values substitution
            stmt.setString(1, company.getComp_name());
            stmt.setString(2, company.getPassword());
            stmt.setString(3, company.getEmail());

            stmt.executeUpdate();

            rs = stmt.getGeneratedKeys();
            
            if (rs.next()) {
                //tries to get generated autoincrement id for newly created company
                company.setId(rs.getLong(1));
            
            }
        } catch (Exception ex) {
            throw new ApplicationException(Exceptions.DATABASE, ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (conn != null) {

                dbConnPoolMgr.returnConnectionToPool(conn);
            }
            
        }
        return company;
    }

    @Override
    public int removeCompany(Long comp_id) throws ApplicationException {

        Connection conn = null;
        PreparedStatement stmt = null;

        int result = 0;

        try {

            //Read SQL statement template from CONFIG
            String sql = ApplicationUtilities.CONFIG.getString("sql.company.delete");

            //get DB connection
            conn = dbConnPoolMgr.getConnectionFromPool();

            stmt = conn.prepareStatement(sql);

            //SQL Values substitution
            stmt.setLong(1, comp_id);

            result = stmt.executeUpdate();

        } catch (Exception ex) {
            throw new ApplicationException(Exceptions.DATABASE, ex);
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (conn != null) {

                dbConnPoolMgr.returnConnectionToPool(conn);

            }

        }

        return result;
    }

    @Override
    public Company updateCompany(Company company) throws ApplicationException {

        Connection conn = null;
        PreparedStatement stmt = null;

        int result = 0;
        try {

            //Read SQL statement template from CONFIG
            String sql = ApplicationUtilities.CONFIG.getString("sql.company.update");

            //get DB connection
            conn = dbConnPoolMgr.getConnectionFromPool();
            stmt = conn.prepareStatement(sql);

            //SQL Values substitution
            stmt.setString(1, company.getPassword());
            stmt.setString(2, company.getEmail());
            stmt.setLong(3, company.getId());

            result = stmt.executeUpdate();

        } catch (Exception ex) {
            throw new ApplicationException(Exceptions.DATABASE, ex);
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (conn != null) {

                dbConnPoolMgr.returnConnectionToPool(conn);

            }

        }

        if (result == 0) {

            throw new ApplicationException(Exceptions.OBJECT_DOESNT_EXIST, "Company with id " + company.getId() + " doesn't exist.");

        }
        

        return company;
    }

    @Override
    public Company getCompany(Long comp_id) throws ApplicationException {

        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Company company = null;

        try {

            //Read SQL statement template from CONFIG
            String sql = ApplicationUtilities.CONFIG.getString("sql.company.read");

            //get DB connection
            conn = dbConnPoolMgr.getConnectionFromPool();
            stmt = conn.prepareStatement(sql);

            //SQL Values substitution
            stmt.setLong(1, comp_id);

            rs = stmt.executeQuery();

            if (rs.next()) {

                company = new Company(
                        rs.getLong("id"),
                        rs.getString("comp_name"),
                        rs.getString("password"),
                        rs.getString("email"),
                        getCoupons(comp_id)
                );

            }

        } catch (Exception ex) {
            throw new ApplicationException(Exceptions.DATABASE, ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (conn != null) {

                dbConnPoolMgr.returnConnectionToPool(conn);

            }
            
        }

        return company;
    }

    @Override
    public Company getCompanyByName(String name) throws ApplicationException {

        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Company company = null;

        try {

            //Read SQL statement template from CONFIG
            String sql = ApplicationUtilities.CONFIG.getString("sql.company.byName");

            //get DB connection
            conn = dbConnPoolMgr.getConnectionFromPool();

            stmt = conn.prepareStatement(sql);

            //SQL Values substitution
            stmt.setString(1, name);

            rs = stmt.executeQuery();

            if (rs.next()) {

                company = new Company(
                        rs.getLong("id"),
                        rs.getString("comp_name"),
                        rs.getString("password"),
                        rs.getString("email")
                );

                company.setCoupons(getCoupons(company.getId()));

            }

        } catch (Exception ex) {
            throw new ApplicationException(Exceptions.DATABASE, ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (conn != null) {

                dbConnPoolMgr.returnConnectionToPool(conn);

            }
            
        }

        return company;
    }

    @Override
    public Collection<Company> getAllCompanies() throws ApplicationException {

        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Collection<Company> companies = new ArrayList<>();

        try {

            //Read SQL statement template from CONFIG
            String sql = ApplicationUtilities.CONFIG.getString("sql.company.all");

            //get DB connection
            conn = dbConnPoolMgr.getConnectionFromPool();
            stmt = conn.prepareStatement(sql);

            rs = stmt.executeQuery();

            while (rs.next()) {

                companies.add(new Company(
                        rs.getLong("id"),
                        rs.getString("comp_name"),
                        rs.getString("password"),
                        rs.getString("email"),
                        getCoupons(rs.getLong("id"))
                ));

            }

        } catch (Exception ex) {
            throw new ApplicationException(Exceptions.DATABASE, ex);
        } finally {
             if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (conn != null) {

                dbConnPoolMgr.returnConnectionToPool(conn);

            }
           
        }

        return companies;
    }

    @Override
    public Coupon getCoupon(Long comp_id, Long coupon_id) throws ApplicationException {

        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Coupon coupon = null;
        try {

            //Read SQL statement template from CONFIG
            String sql = ApplicationUtilities.CONFIG.getString("sql.company.coupons.one");

            //get DB connection
            conn = dbConnPoolMgr.getConnectionFromPool();
            stmt = conn.prepareStatement(sql);

            stmt.setLong(1, comp_id);
            stmt.setLong(2, coupon_id);

            rs = stmt.executeQuery();

            while (rs.next()) {

                coupon = new Coupon(
                        rs.getLong("id"),
                        rs.getString("title"),
                        rs.getDate("startDate"),
                        rs.getDate("endDate"),
                        rs.getInt("amount"),
                        Category.fromInt(rs.getInt("type")),
                        rs.getString("message"),
                        rs.getDouble("price"),
                        rs.getString("image")
                );

            }

        } catch (Exception ex) {
            throw new ApplicationException(Exceptions.DATABASE, ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (conn != null) {

                dbConnPoolMgr.returnConnectionToPool(conn);

            }
            

        }
        return coupon;
    }

    @Override
    public Collection<Coupon> getCoupons(Long id) throws ApplicationException {

        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Collection<Coupon> coupons = new ArrayList<>();

        try {

            //Read SQL statement template from CONFIG
            String sql = ApplicationUtilities.CONFIG.getString("sql.company.coupons.all");

            //get DB connection
            conn = dbConnPoolMgr.getConnectionFromPool();
            stmt = conn.prepareStatement(sql);

            stmt.setLong(1, id);

            rs = stmt.executeQuery();

            while (rs.next()) {

                coupons.add(new Coupon(
                        rs.getLong("id"),
                        rs.getString("title"),
                        rs.getDate("startDate"),
                        rs.getDate("endDate"),
                        rs.getInt("amount"),
                        Category.fromInt(rs.getInt("type")),
                        rs.getString("message"),
                        rs.getDouble("price"),
                        rs.getString("image")
                ));

            }

        } catch (Exception ex) {
            throw new ApplicationException(Exceptions.DATABASE, ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (conn != null) {

                dbConnPoolMgr.returnConnectionToPool(conn);

            }

        }

        return coupons;
    }

    @Override
    public Collection<Coupon> getCoupons(Long id, Category type) throws ApplicationException {

        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Collection<Coupon> coupons = new ArrayList<>();

        try {

            //Read SQL statement template from CONFIG
            String sql = ApplicationUtilities.CONFIG.getString("sql.company.coupons.byType");

            //get DB connection
            conn = dbConnPoolMgr.getConnectionFromPool();
            stmt = conn.prepareStatement(sql);

            stmt.setLong(1, id);
            stmt.setInt(2, type.getValue());

            rs = stmt.executeQuery();

            while (rs.next()) {

                coupons.add(new Coupon(
                        rs.getLong("id"),
                        rs.getString("title"),
                        rs.getDate("startDate"),
                        rs.getDate("endDate"),
                        rs.getInt("amount"),
                        Category.fromInt(rs.getInt("type")),
                        rs.getString("message"),
                        rs.getDouble("price"),
                        rs.getString("image")
                ));

            }

        } catch (Exception ex) {
            throw new ApplicationException(Exceptions.DATABASE, ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (conn != null) {

                dbConnPoolMgr.returnConnectionToPool(conn);

            }

        }

        return coupons;
    }

    @Override
    public Collection<Coupon> getCoupons(Long id, double price) throws ApplicationException {

        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Collection<Coupon> coupons = new ArrayList<>();

        try {

            //Read SQL statement template from CONFIG
            String sql = ApplicationUtilities.CONFIG.getString("sql.company.coupons.byPrice");

            //get DB connection
            conn = dbConnPoolMgr.getConnectionFromPool();
            stmt = conn.prepareStatement(sql);

            stmt.setLong(1, id);
            stmt.setDouble(2, price);

            rs = stmt.executeQuery();

            while (rs.next()) {

                coupons.add(new Coupon(
                        rs.getLong("id"),
                        rs.getString("title"),
                        rs.getDate("startDate"),
                        rs.getDate("endDate"),
                        rs.getInt("amount"),
                        Category.fromInt(rs.getInt("type")),
                        rs.getString("message"),
                        rs.getDouble("price"),
                        rs.getString("image")
                ));

            }

        } catch (Exception ex) {
            throw new ApplicationException(Exceptions.DATABASE, ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (conn != null) {

                dbConnPoolMgr.returnConnectionToPool(conn);

            }

        }

        return coupons;
    }

    @Override
    public Collection<Coupon> getCoupons(Long id, Date date) throws ApplicationException {

        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Collection<Coupon> coupons = new ArrayList<>();

        try {

            //Read SQL statement template from CONFIG
            String sql = ApplicationUtilities.CONFIG.getString("sql.company.coupons.byDate");

            //get DB connection
            conn = dbConnPoolMgr.getConnectionFromPool();
            stmt = conn.prepareStatement(sql);

            stmt.setLong(1, id);
            stmt.setDate(2, date);

            rs = stmt.executeQuery();

            while (rs.next()) {

                coupons.add(new Coupon(
                        rs.getLong("id"),
                        rs.getString("title"),
                        rs.getDate("startDate"),
                        rs.getDate("endDate"),
                        rs.getInt("amount"),
                        Category.fromInt(rs.getInt("type")),
                        rs.getString("message"),
                        rs.getDouble("price"),
                        rs.getString("image")
                ));

            }

        } catch (Exception ex) {
            throw new ApplicationException(Exceptions.DATABASE, ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (conn != null) {

                dbConnPoolMgr.returnConnectionToPool(conn);

            }

        }

        return coupons;
    }

    @Override
    public int removeAllCoupons(Long comp_id) throws ApplicationException {

        Connection conn = null;
        PreparedStatement stmt = null;

        int result = 0;
        try {

            //Read SQL statement template from CONFIG
            String sql = ApplicationUtilities.CONFIG.getString("sql.company.coupons.removeAll");

            //get DB connection
            conn = dbConnPoolMgr.getConnectionFromPool();
            stmt = conn.prepareStatement(sql);

            //SQL Values substitution
            stmt.setLong(1, comp_id);

            result = stmt.executeUpdate();

        } catch (Exception ex) {
            throw new ApplicationException(Exceptions.DATABASE, ex);
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (conn != null) {

                dbConnPoolMgr.returnConnectionToPool(conn);

            }

        }

        return result;
    }

    @Override
    public int removeCoupon(Long coupon_id) throws ApplicationException {

        Connection conn = null;
        PreparedStatement stmt = null;

        int result = 0;

        try {

            //Read SQL statement template from CONFIG
            String sql = ApplicationUtilities.CONFIG.getString("sql.company.coupons.removeById");

            //get DB connection
            conn = dbConnPoolMgr.getConnectionFromPool();
            stmt = conn.prepareStatement(sql);

            //SQL Values substitution
            stmt.setLong(1, coupon_id);

            result = stmt.executeUpdate();

        } catch (Exception ex) {
            throw new ApplicationException(Exceptions.DATABASE, ex);
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (conn != null) {

                dbConnPoolMgr.returnConnectionToPool(conn);

            }

        }

        return result;
    }

    @Override
    public int couponCreated(Long comp_id, Long coupon_id) throws ApplicationException {

        Connection conn = null;
        PreparedStatement stmt = null;

        int result = 0;

        try {

            //Read SQL statement template from CONFIG
            String sql = CONFIG.getString("sql.company.coupons.insert");

            //get DB connection
            conn = dbConnPoolMgr.getConnectionFromPool();

            stmt = conn.prepareStatement(sql);

            //SQL Values substitution 
            stmt.setLong(1, comp_id);
            stmt.setLong(2, coupon_id);

            result = stmt.executeUpdate();

        } catch (Exception ex) {
            throw new ApplicationException(Exceptions.DATABASE, ex);
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (conn != null) {

                dbConnPoolMgr.returnConnectionToPool(conn);

            }
        }

        return result;
    }

    @Override
    public boolean login(String username, String password) throws ApplicationException {

        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {

            //Read SQL statement template from CONFIG
            String sql = ApplicationUtilities.CONFIG.getString("sql.company.login");

            //get DB connection
            conn = dbConnPoolMgr.getConnectionFromPool();
            stmt = conn.prepareStatement(sql);

            //SQL Values suTODObstitution
            stmt.setString(1, username);
            stmt.setString(2, password);

            rs = stmt.executeQuery();

            if (rs.next()) {
                return true;
            }

        } catch (Exception ex) {
            throw new ApplicationException(Exceptions.DATABASE, ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (conn != null) {

                dbConnPoolMgr.returnConnectionToPool(conn);

            }

        }

        return false;
    }

}
