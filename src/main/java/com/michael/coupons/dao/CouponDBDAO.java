package com.michael.coupons.dao;

import com.michael.coupons.dao.interfaces.ICouponDAO;
import com.michael.coupons.beans.Coupon;
import com.michael.coupons.database.DBConnetionPoolManagerFactory;
import com.michael.coupons.database.IDBConnectionPoolManager;
import com.michael.coupons.entities.Category;
import com.michael.coupons.exceptions.ApplicationException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import static com.michael.coupons.utils.ApplicationUtilities.CONFIG;
import java.sql.Statement;

public class CouponDBDAO implements ICouponDAO {

    // ======================CONSTANTS========================
    private static final long serialVersionUID = 1L;
    private final IDBConnectionPoolManager dbConnPoolMgr;
    
    // ==============SINGLETONE======================
    private static final CouponDBDAO INSTANCE = new CouponDBDAO();
    
      private CouponDBDAO() {
        this.dbConnPoolMgr = DBConnetionPoolManagerFactory.getInstance();
    }
    
    public static CouponDBDAO getInstance() {
        return INSTANCE;
    }

  
    @Override
    public Coupon createCoupon(Coupon coupon) throws ApplicationException {
         
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {

            //Read SQL statement template from CONFIG
            String sql = CONFIG.getString("sql.coupon.create");

            //get DB connection
            conn = dbConnPoolMgr.getConnectionFromPool();

            stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            //SQL Values substitution 
            stmt.setString(1, coupon.getTitle());
            stmt.setDate(2, coupon.getStartDate());
            stmt.setDate(3, coupon.getEndDate());
            stmt.setInt(4, coupon.getAmount());
            stmt.setInt(5, coupon.getType());
            stmt.setString(6, coupon.getMessage());
            stmt.setDouble(7, coupon.getPrice());
            stmt.setString(8, coupon.getImage());

            stmt.executeUpdate();
            
            rs = stmt.getGeneratedKeys();
            
            if (rs.next()) {
            
            //tries to get generated autoincrement id for newly created company
             coupon.setId(rs.getLong(1));
            }

        } catch (Exception ex) {
            throw new ApplicationException(ex);
        } finally {
             if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (conn != null) {

                dbConnPoolMgr.returnConnectionToPool(conn);

            }
        }

        return coupon;
    }

    @Override
    public int removeCoupon(Long coupon_id) throws ApplicationException {
         
        Connection conn = null;
        PreparedStatement stmt = null;
        
        int result = 0;

        try {

            //Read SQL statement template from CONFIG
            String sql = CONFIG.getString("sql.coupon.delete");

            //get DB connection
            conn = dbConnPoolMgr.getConnectionFromPool();

            stmt = conn.prepareStatement(sql);

            //SQL Values substitution 
            stmt.setLong(1, coupon_id);

            result = stmt.executeUpdate();

        } catch (Exception ex) {
            throw new ApplicationException(ex);
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (conn != null) {

                dbConnPoolMgr.returnConnectionToPool(conn);

            }
        }

        return result;
    }

    @Override
    public Coupon updateCoupon(Coupon coupon) throws ApplicationException {
         
        Connection conn = null;
        PreparedStatement stmt = null;
        
        try {

            //Read SQL statement template from CONFIG
            String sql = CONFIG.getString("sql.coupon.update");

            //get DB connection
            conn = dbConnPoolMgr.getConnectionFromPool();

            stmt = conn.prepareStatement(sql);

            //SQL Values substitution 
            stmt.setString(1, coupon.getTitle());
            stmt.setDate(2, coupon.getStartDate());
            stmt.setDate(3, coupon.getEndDate());
            stmt.setInt(4, coupon.getAmount());
            stmt.setInt(5, coupon.getType());
            stmt.setString(6, coupon.getMessage());
            stmt.setDouble(7, coupon.getPrice());
            stmt.setString(8, coupon.getImage());
            stmt.setLong(9, coupon.getId());

            stmt.executeUpdate();

        } catch (Exception ex) {
            throw new ApplicationException(ex);
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (conn != null) {

                dbConnPoolMgr.returnConnectionToPool(conn);

            }
        }

        return coupon;
    }

    @Override
    public Coupon getCoupon(Long couponId) throws ApplicationException {
         
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Coupon coupon = null;

        try {

            //Read SQL statement template from CONFIG
            String sql = CONFIG.getString("sql.coupon.read");

            //get DB connection
            conn = dbConnPoolMgr.getConnectionFromPool();

            stmt = conn.prepareStatement(sql);

            //SQL Values substitution 
            stmt.setLong(1, couponId);

            rs = stmt.executeQuery();

            if (rs.next()) {

                coupon = new Coupon(
                        rs.getLong("id"),
                        rs.getString("title"),
                        rs.getDate("startDate"),
                        rs.getDate("endDate"),
                        rs.getInt("amount"),
                        Category.fromInt(rs.getInt("type")),
                        rs.getString("message"),
                        rs.getDouble("price"),
                        rs.getString("image")
                );

            }

        } catch (Exception ex) {
            throw new ApplicationException(ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (conn != null) {

                dbConnPoolMgr.returnConnectionToPool(conn);

            }
        }

        return coupon;
    }
    
    @Override
    public Coupon getCouponByTitle(String title) throws ApplicationException {
           
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Coupon coupon = null;

        try {

            //Read SQL statement template from CONFIG
            String sql = CONFIG.getString("sql.coupon.byTitle");

            //get DB connection
            conn = dbConnPoolMgr.getConnectionFromPool();

            stmt = conn.prepareStatement(sql);

            //SQL Values substitution 
            stmt.setString(1, title);

            rs = stmt.executeQuery();

            if (rs.next()) {

                coupon = new Coupon(
                        rs.getLong("id"),
                        rs.getString("title"),
                        rs.getDate("startDate"),
                        rs.getDate("endDate"),
                        rs.getInt("amount"),
                        Category.fromInt(rs.getInt("type")),
                        rs.getString("message"),
                        rs.getDouble("price"),
                        rs.getString("image")
                );

            }

        } catch (Exception ex) {
            throw new ApplicationException(ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (conn != null) {

                dbConnPoolMgr.returnConnectionToPool(conn);

            }
        }

        return coupon;
    }

    @Override
    public Collection<Coupon> getAllCoupons() throws ApplicationException {
         
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Collection<Coupon> coupons = new ArrayList<>();

        try {

            //Read SQL statement template from CONFIG
            String sql = CONFIG.getString("sql.coupon.all");

            //get DB connection
            conn = dbConnPoolMgr.getConnectionFromPool();

            stmt = conn.prepareStatement(sql);

            rs = stmt.executeQuery();

            while (rs.next()) {

                coupons.add(new Coupon(
                        rs.getLong("id"),
                        rs.getString("title"),
                        rs.getDate("startDate"),
                        rs.getDate("endDate"),
                        rs.getInt("amount"),
                        Category.fromInt(rs.getInt("type")),
                        rs.getString("message"),
                        rs.getDouble("price"),
                        rs.getString("image")
                ));

            }

        } catch (Exception ex) {
            throw new ApplicationException(ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (conn != null) {

                dbConnPoolMgr.returnConnectionToPool(conn);

            }
        }

        return coupons;
    }

    @Override
    public List<Long> getCustomersIdsByCouponId(Long couponId) throws ApplicationException {
         
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        List<Long> customersIds = new ArrayList<>();

        try {

            //Read SQL statement template from CONFIG
            String sql = CONFIG.getString("sql.coupon.customers");

            //get DB connection
            conn = dbConnPoolMgr.getConnectionFromPool();

            stmt = conn.prepareStatement(sql);

            //SQL Values substitution 
            stmt.setLong(1, couponId);

            rs = stmt.executeQuery();

            while (rs.next()) {

                customersIds.add(rs.getLong("id"));

            }

        } catch (Exception ex) {
            throw new ApplicationException(ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (conn != null) {

                dbConnPoolMgr.returnConnectionToPool(conn);

            }
        }

        if (customersIds.isEmpty()) {
            return null;
        } else {
            return customersIds;
        }
    }

    @Override
    public Long getCompanyIdByCouponId(Long couponId) throws ApplicationException {
         
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Long comp_id = null;

        try {

            //Read SQL statement template from CONFIG
            String sql = CONFIG.getString("sql.coupon.company");

            //get DB connection
            conn = dbConnPoolMgr.getConnectionFromPool();

            stmt = conn.prepareStatement(sql);

            //SQL Values substitution 
            stmt.setLong(1, couponId);

            rs = stmt.executeQuery();

            while (rs.next()) {

                comp_id = rs.getLong("id");
            }

        } catch (Exception ex) {
            throw new ApplicationException(ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (conn != null) {

                dbConnPoolMgr.returnConnectionToPool(conn);

            }
        }

        return comp_id;

    }

    @Override
    public Collection<Coupon> getExpiredCoupons(java.sql.Date date) throws ApplicationException {
         
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Collection<Coupon> coupons = new ArrayList<>();

        try {

            //Read SQL statement template from CONFIG
            String sql = CONFIG.getString("sql.coupon.expired");

            //get DB connection
            conn = dbConnPoolMgr.getConnectionFromPool();

            stmt = conn.prepareStatement(sql);

            stmt.setDate(1, date);
            
            rs = stmt.executeQuery();

            while (rs.next()) {

                coupons.add(new Coupon(
                        rs.getLong("id"),
                        rs.getString("title"),
                        rs.getDate("startDate"),
                        rs.getDate("endDate"),
                        rs.getInt("amount"),
                        Category.fromInt(rs.getInt("type")),
                        rs.getString("message"),
                        rs.getDouble("price"),
                        rs.getString("image")
                ));

            }

        } catch (Exception ex) {
            throw new ApplicationException(ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (conn != null) {

                dbConnPoolMgr.returnConnectionToPool(conn);

            }
        }

        return coupons;
    }
}
