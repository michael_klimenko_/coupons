package com.michael.coupons.dao;

import com.michael.coupons.exceptions.ApplicationException;
import com.michael.coupons.utils.Exceptions;
import com.michael.coupons.dao.interfaces.ICustomerDAO;
import com.michael.coupons.beans.Coupon;
import com.michael.coupons.beans.Customer;
import com.michael.coupons.database.DBConnetionPoolManagerFactory;
import com.michael.coupons.database.IDBConnectionPoolManager;
import com.michael.coupons.entities.Category;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import static com.michael.coupons.utils.ApplicationUtilities.CONFIG;
import java.sql.Statement;

public class CustomerDBDAO implements ICustomerDAO {

    // ======================CONSTANTS========================
    private static final long serialVersionUID = 1L;
    private final IDBConnectionPoolManager dbConnPoolMgr;

    // ==============SINGLETONE======================
    private static final CustomerDBDAO INSTANCE = new CustomerDBDAO();
    
    private CustomerDBDAO() {
        this.dbConnPoolMgr = DBConnetionPoolManagerFactory.getInstance();
    }

    public static CustomerDBDAO getInstance() {
        return INSTANCE;
    }

    @Override
    public Customer createCustomer(Customer customer) throws ApplicationException {

        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {

            //Read SQL statement template from CONFIG
            String sql = CONFIG.getString("sql.customer.create");

            //get DB connection
            conn = dbConnPoolMgr.getConnectionFromPool();

            stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            //SQL Values substitution 
            stmt.setString(1, customer.getCust_name());
            stmt.setString(2, customer.getPassword());

            stmt.executeUpdate();

            rs = stmt.getGeneratedKeys();
            //tries to get generated autoincrement id for newly created company
            if (rs.next()) {

                customer.setId(rs.getLong(1));
            }

        } catch (Exception ex) {
            throw new ApplicationException(Exceptions.DATABASE, ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (conn != null) {

                dbConnPoolMgr.returnConnectionToPool(conn);

            }
        }

        return customer;
    }

    @Override
    public int removeCustomer(Long cust_id) throws ApplicationException {

        Connection conn = null;
        PreparedStatement stmt = null;

        int result = 0;
        try {

            //Read SQL statement template from CONFIG
            String sql = CONFIG.getString("sql.customer.delete");

            //get DB connection
            conn = dbConnPoolMgr.getConnectionFromPool();

            stmt = conn.prepareStatement(sql);

            //SQL Values substitution 
            stmt.setLong(1, cust_id);

            result = stmt.executeUpdate();

        } catch (Exception ex) {
            throw new ApplicationException(Exceptions.DATABASE, ex);
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (conn != null) {

                dbConnPoolMgr.returnConnectionToPool(conn);

            }
        }

        return result;
    }

    @Override
    public Customer updateCustomer(Customer customer) throws ApplicationException {

        Connection conn = null;
        PreparedStatement stmt = null;

        try {

            //Read SQL statement template from CONFIG
            String sql = CONFIG.getString("sql.customer.update");

            //get DB connection
            conn = dbConnPoolMgr.getConnectionFromPool();

            stmt = conn.prepareStatement(sql);

            //SQL Values substitution 
            stmt.setString(1, customer.getPassword());
            stmt.setLong(2, customer.getId());

            stmt.executeUpdate();

        } catch (Exception ex) {
            throw new ApplicationException(Exceptions.DATABASE, ex);
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (conn != null) {

                dbConnPoolMgr.returnConnectionToPool(conn);

            }
        }

        return customer;
    }

    @Override
    public Customer getCustomer(Long cust_id) throws ApplicationException {

        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Customer customer = null;

        try {

            //Read SQL statement template from CONFIG
            String sql = CONFIG.getString("sql.customer.read");

            //get DB connection
            conn = dbConnPoolMgr.getConnectionFromPool();

            stmt = conn.prepareStatement(sql);

            //SQL Values suTODObstitution 
            stmt.setLong(1, cust_id);

            rs = stmt.executeQuery();

            if (rs.next()) {

                customer = new Customer(
                        rs.getLong("id"),
                        rs.getString("cust_name"),
                        rs.getString("password"),
                        getCoupons(cust_id)
                );

            }

        } catch (Exception ex) {
            throw new ApplicationException(Exceptions.DATABASE, ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (conn != null) {

                dbConnPoolMgr.returnConnectionToPool(conn);

            }
        }

        return customer;
    }

    @Override
    public Customer getCustomerByName(String name) throws ApplicationException {

        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Customer customer = null;

        try {

            //Read SQL statement template from CONFIG
            String sql = CONFIG.getString("sql.customer.byName");

            //get DB connection
            conn = dbConnPoolMgr.getConnectionFromPool();

            stmt = conn.prepareStatement(sql);

            //SQL Values suTODObstitution 
            stmt.setString(1, name);

            rs = stmt.executeQuery();

            if (rs.next()) {

                customer = new Customer(
                        rs.getLong("id"),
                        rs.getString("cust_name"),
                        rs.getString("password"),
                        getCoupons(rs.getLong("id"))
                );

            }

        } catch (Exception ex) {
            throw new ApplicationException(Exceptions.DATABASE, ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (conn != null) {

                dbConnPoolMgr.returnConnectionToPool(conn);

            }
        }

        return customer;
    }

    @Override
    public Collection<Customer> getAllCustomers() throws ApplicationException {

        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Collection<Customer> customers = new ArrayList<>();

        try {

            //Read SQL statement template from CONFIG
            String sql = CONFIG.getString("sql.customer.all");

            //get DB connection
            conn = dbConnPoolMgr.getConnectionFromPool();

            stmt = conn.prepareStatement(sql);

            rs = stmt.executeQuery();

            while (rs.next()) {

                customers.add(new Customer(
                        rs.getLong("id"),
                        rs.getString("cust_name"),
                        rs.getString("password"),
                        getCoupons(rs.getLong("id"))
                ));

            }

        } catch (Exception ex) {
            throw new ApplicationException(Exceptions.DATABASE, ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (conn != null) {

                dbConnPoolMgr.returnConnectionToPool(conn);

            }
        }

        return customers;

    }

    @Override
    public Coupon getCoupon(Long cust_id, Long coupon_id) throws ApplicationException {

        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Coupon coupon = null;
        try {

            //Read SQL statement template from CONFIG
            String sql = CONFIG.getString("sql.customer.coupons.one");

            //get DB connection
            conn = dbConnPoolMgr.getConnectionFromPool();

            stmt = conn.prepareStatement(sql);

            stmt.setLong(1, cust_id);
            stmt.setLong(2, coupon_id);

            rs = stmt.executeQuery();

            while (rs.next()) {

                coupon = new Coupon(
                        rs.getLong("id"),
                        rs.getString("title"),
                        rs.getDate("startDate"),
                        rs.getDate("endDate"),
                        rs.getInt("amount"),
                        Category.fromInt(rs.getInt("type")),
                        rs.getString("message"),
                        rs.getDouble("price"),
                        rs.getString("image")
                );

            }

        } catch (Exception ex) {
            throw new ApplicationException(Exceptions.DATABASE, ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (conn != null) {

                dbConnPoolMgr.returnConnectionToPool(conn);

            }
        }
        return coupon;
    }

    @Override
    public Collection<Coupon> getCoupons(Long cust_id) throws ApplicationException {

        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Collection<Coupon> coupons = new ArrayList<>();

        try {

            //Read SQL statement template from CONFIG
            String sql = CONFIG.getString("sql.customer.coupons.all");

            //get DB connection
            conn = dbConnPoolMgr.getConnectionFromPool();

            stmt = conn.prepareStatement(sql);

            stmt.setLong(1, cust_id);

            rs = stmt.executeQuery();

            while (rs.next()) {

                coupons.add(new Coupon(
                        rs.getLong("id"),
                        rs.getString("title"),
                        rs.getDate("startDate"),
                        rs.getDate("endDate"),
                        rs.getInt("amount"),
                        Category.fromInt(rs.getInt("type")),
                        rs.getString("message"),
                        rs.getDouble("price"),
                        rs.getString("image")
                ));

            }

        } catch (Exception ex) {
            throw new ApplicationException(Exceptions.DATABASE, ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (conn != null) {

                dbConnPoolMgr.returnConnectionToPool(conn);

            }
        }

        return coupons;
    }

    @Override
    public Collection<Coupon> getCoupons(Long cust_id, Category type) throws ApplicationException {

        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Collection<Coupon> coupons = new ArrayList<>();

        try {

            //Read SQL statement template from CONFIG
            String sql = CONFIG.getString("sql.customer.coupons.byType");

            //get DB connection
            conn = dbConnPoolMgr.getConnectionFromPool();

            stmt = conn.prepareStatement(sql);

            stmt.setLong(1, cust_id);
            stmt.setInt(2, type.getValue());

            rs = stmt.executeQuery();

            while (rs.next()) {

                coupons.add(new Coupon(
                        rs.getLong("id"),
                        rs.getString("title"),
                        rs.getDate("startDate"),
                        rs.getDate("endDate"),
                        rs.getInt("amount"),
                        Category.fromInt(rs.getInt("type")),
                        rs.getString("message"),
                        rs.getDouble("price"),
                        rs.getString("image")
                ));

            }

        } catch (Exception ex) {
            throw new ApplicationException(Exceptions.DATABASE, ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (conn != null) {

                dbConnPoolMgr.returnConnectionToPool(conn);

            }
        }

        return coupons;
    }

    @Override
    public Collection<Coupon> getCoupons(Long cust_id, double price) throws ApplicationException {

        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Collection<Coupon> coupons = new ArrayList<>();

        try {

            //Read SQL statement template from CONFIG
            String sql = CONFIG.getString("sql.customer.coupons.byPrice");

            //get DB connection
            conn = dbConnPoolMgr.getConnectionFromPool();

            stmt = conn.prepareStatement(sql);

            stmt.setLong(1, cust_id);
            stmt.setDouble(2, price);

            rs = stmt.executeQuery();

            while (rs.next()) {

                coupons.add(new Coupon(
                        rs.getLong("id"),
                        rs.getString("title"),
                        rs.getDate("startDate"),
                        rs.getDate("endDate"),
                        rs.getInt("amount"),
                        Category.fromInt(rs.getInt("type")),
                        rs.getString("message"),
                        rs.getDouble("price"),
                        rs.getString("image")
                ));

            }

        } catch (Exception ex) {
            throw new ApplicationException(Exceptions.DATABASE, ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (conn != null) {

                dbConnPoolMgr.returnConnectionToPool(conn);

            }
        }

        return coupons;
    }

    @Override
    public int removeAllCoupons(Long cust_id) throws ApplicationException {

        Connection conn = null;
        PreparedStatement stmt = null;

        int result = 0;
        try {

            //Read SQL statement template from CONFIG
            String sql = CONFIG.getString("sql.customer.coupons.removeAll");

            //get DB connection
            conn = dbConnPoolMgr.getConnectionFromPool();

            stmt = conn.prepareStatement(sql);

            //SQL Values substitution 
            stmt.setLong(1, cust_id);

            result = stmt.executeUpdate();

        } catch (Exception ex) {
            throw new ApplicationException(Exceptions.DATABASE, ex);
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (conn != null) {

                dbConnPoolMgr.returnConnectionToPool(conn);

            }
        }

        return result;
    }

    @Override
    public int removeCoupon(Long coupon_id) throws ApplicationException {

        Connection conn = null;
        PreparedStatement stmt = null;

        int result = 0;

        try {

            //Read SQL statement template from CONFIG
            String sql = CONFIG.getString("sql.customer.coupons.removeById");

            //get DB connection
            conn = dbConnPoolMgr.getConnectionFromPool();

            stmt = conn.prepareStatement(sql);

            //SQL Values substitution 
            stmt.setLong(1, coupon_id);

            result = stmt.executeUpdate();

        } catch (Exception ex) {
            throw new ApplicationException(Exceptions.DATABASE, ex);
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (conn != null) {

                dbConnPoolMgr.returnConnectionToPool(conn);

            }
        }

        return result;
    }

    @Override
    public int couponPurchased(Long cust_id, Long coupon_id) throws ApplicationException {

        Connection conn = null;
        PreparedStatement stmt = null;

        int result = 0;

        try {

            //Read SQL statement template from CONFIG
            String sql = CONFIG.getString("sql.customer.coupons.insert");

            //get DB connection
            conn = dbConnPoolMgr.getConnectionFromPool();

            stmt = conn.prepareStatement(sql);

            //SQL Values substitution 
            stmt.setLong(1, cust_id);
            stmt.setLong(2, coupon_id);

            result = stmt.executeUpdate();

        } catch (Exception ex) {
            throw new ApplicationException(Exceptions.DATABASE, ex);
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (conn != null) {

                dbConnPoolMgr.returnConnectionToPool(conn);

            }
        }

        return result;
    }

    @Override
    public boolean login(String username, String password) throws ApplicationException {

        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {

            //Read SQL statement template from CONFIG
            String sql = CONFIG.getString("sql.customer.login");

            //get DB connection
            conn = dbConnPoolMgr.getConnectionFromPool();

            stmt = conn.prepareStatement(sql);

            //SQL Values substitution 
            stmt.setString(1, username);
            stmt.setString(2, password);

            rs = stmt.executeQuery();

            if (rs.next()) {
                return true;
            }

        } catch (Exception ex) {
            throw new ApplicationException(Exceptions.DATABASE, ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    throw new ApplicationException(ex);
                }
            }
            if (conn != null) {

                dbConnPoolMgr.returnConnectionToPool(conn);

            }
        }

        return false;
    }
}
