package com.michael.coupons.dao.interfaces;

import com.michael.coupon.web.utils.cgrates.Account;
import com.michael.coupon.web.utils.cgrates.AttrGetAccount;
import com.michael.coupon.web.utils.cgrates.AttrGetAccounts;
import com.michael.coupons.exceptions.ApplicationException;


public interface IAccountDAO {
 
    public Account[] getAccounts(AttrGetAccounts attrGetAccounts) throws ApplicationException;
    public Account getAccount(AttrGetAccount attrGetAccount) throws ApplicationException;
    
}
