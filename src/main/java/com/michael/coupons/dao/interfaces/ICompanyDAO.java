package com.michael.coupons.dao.interfaces;

import com.michael.coupons.beans.Company;
import com.michael.coupons.beans.Coupon;
import com.michael.coupons.entities.Category;
import com.michael.coupons.exceptions.ApplicationException;
import java.io.Serializable;
import java.sql.Date;
import java.util.Collection;

/**
 * ICompanyDAO interface is implemented by persistence mechanism class.
 * Includes declaration of necessary DAO actions for com.michael.coupons.beans.Company model.
 * 
 * @author michael
 */
public interface ICompanyDAO extends Serializable {

    /**
     * Method tries to create a new company.
     * @param company The Company object populated with values.
     * @return Company created.
     * @throws ApplicationException in case of error.
     */
    public Company createCompany(Company company) throws ApplicationException;

    /**
     * Method tries to remove existing company.
     * @param comp_id The company ID.
     * @return number of companies removed.
     * @throws ApplicationException in case of error.
     */
    public int removeCompany(Long comp_id) throws ApplicationException;

    /**
     * Method tries to update Company object.
     * @param company The Company object with changed property.
     * @return Company changed.
     * @throws ApplicationException in case of error.
     */
    public Company updateCompany(Company company) throws ApplicationException;

    /**
     * Method tries to retrieve a company by ID.
     * @param comp_id Company ID.
     * @return Company object if exists.
     * @throws ApplicationException in case of error.
     */
    public Company getCompany(Long comp_id) throws ApplicationException;
    
    /**
     * Method retrieves a company by name.
     * @param name Company name.
     * @return Company object if exists.
     * @throws ApplicationException in case of error.
     */
    public Company getCompanyByName(String name) throws ApplicationException;

    /**
     * Method tries to retrieve all the companies .
     * @return Collection of Company objects.
     * @throws ApplicationException in case of error.
     */
    public Collection<Company> getAllCompanies() throws ApplicationException;
    
    /**
     * Method tries to retrieve a coupon by Company ID and Coupon ID.
     * @param comp_id Company ID.
     * @param coupon_id Coupon ID.
     * @return Coupon object if exists.
     * @throws ApplicationException in case of error.
     */
    public Coupon getCoupon(Long comp_id, Long coupon_id) throws ApplicationException;
    
    /**
     * Method tries to retrieve all coupons that belongs to a company by company id.
     * @param comp_id Company ID.
     * @return Collection of Coupon objects.
     * @throws ApplicationException in case of error.
     */
    public Collection<Coupon> getCoupons(Long comp_id) throws ApplicationException;
    
    /**
     * Method tries to retrieve all coupons that belongs to a company by company id and of specific type.
     * @param comp_id Company ID.
     * @param type Category type.
     * @return Collection of Coupon objects.
     * @throws ApplicationException in case of error.
     */
    public Collection<Coupon> getCoupons(Long comp_id, Category type) throws ApplicationException;
    
    /**
     * Method tries to retrieve all coupons that belongs to a company by company id and up to specified price.
     * @param comp_id Company ID.
     * @param price Coupon Price.
     * @return Collection of Coupon objects.
     * @throws ApplicationException in case of error.
     */
    public Collection<Coupon> getCoupons(Long comp_id, double price) throws ApplicationException;
    
    /**
     * Method tries to retrieve all coupons that belongs to a company by company id and up to specified date.
     * @param comp_id Company ID.
     * @param date The coupon expiration date.
     * @return Collection of Coupon objects.
     * @throws ApplicationException in case of error.
     */
    public Collection<Coupon> getCoupons(Long comp_id, Date date) throws ApplicationException;
    
    /**
     * Method tries to remove all coupons belonging to specific company by company ID.
     * @param comp_id Company ID.
     * @return number of coupons removed.
     * @throws ApplicationException in case of error.
     */ 
    public int removeAllCoupons(Long comp_id) throws ApplicationException;
    
    /**
     * Method tries to remove coupon reference from company_coupons.
     * @param coupon_id Coupon ID.
     * @return number of removed coupon references.
     * @throws ApplicationException in case of error.
     */
    public int removeCoupon(Long coupon_id) throws ApplicationException;
    
    /**
     * Method tries to create coupon reference in company_coupons.
     * @param comp_id Company ID.
     * @param coupon_id Coupon ID.
     * @return number of created coupon references.
     * @throws ApplicationException in case of error.
     */
    public int couponCreated(Long comp_id, Long coupon_id) throws ApplicationException; 

    /**
     * Method tries to verify provided credentials do match.
     * @param username The Username.
     * @param password The Password.
     * @return <code>true</code> in case of username/password do match;
     * <code>false</code> otherwise.
     * @throws ApplicationException in case of error.
     */
    public boolean login(String username, String password) throws ApplicationException;

}
