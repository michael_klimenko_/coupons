package com.michael.coupons.dao.interfaces;

import com.michael.coupons.beans.Coupon;
import com.michael.coupons.exceptions.ApplicationException;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * ICouponDAO interface is implemented by persistence mechanism class.
 * Includes declaration of necessary DAO actions for com.michael.coupons.beans.Coupon model.
 * 
 * @author michael
 */
public interface ICouponDAO extends Serializable {

    /**
     * Method tries to create coupon.
     * @param coupon The Coupon object populated with values.
     * @return Coupon created.
     * @throws ApplicationException in case of error.
     */
    public Coupon createCoupon(Coupon coupon) throws ApplicationException;

    /**
     * Method tries to remove coupon.
     * @param coupon_id The Coupon ID
     * @return number of removed coupons.
     * @throws ApplicationException in case of error.
     */
    public int removeCoupon(Long coupon_id) throws ApplicationException;

    /**
     * Method tries to update coupon attributes.
     * @param coupon Coupon with updated values.
     * @return Coupon updated.
     * @throws ApplicationException in case of error.
     */
    public Coupon updateCoupon(Coupon coupon) throws ApplicationException;

    /**
     * Method tries to retrieve coupon by coupon ID.
     * @param coupon_id The Coupon ID.
     * @return Coupon object if exists.
     * @throws ApplicationException in case of error.
     */
    public Coupon getCoupon(Long coupon_id) throws ApplicationException;
    
    /**
     * Method tries to retrieve coupon by it's title.
     * @param title The Coupon title.
     * @return Coupon object if exists.
     * @throws ApplicationException in case of error.
     */
    public Coupon getCouponByTitle(String title) throws ApplicationException;
    
    /**
     * Method tries to retrieve all existing coupons.
     * @return Collection of Coupon objects.
     * @throws ApplicationException in case of error.
     */
    public Collection<Coupon> getAllCoupons() throws ApplicationException;

    /**
      Method tries to retrieve a list of customer IDs that purchased specific coupon. 
     * @param coupon_id The Coupon ID.
     * @return List of customer Ids if any.
     * @throws ApplicationException in case of error.
     */
    public List<Long> getCustomersIdsByCouponId(Long coupon_id) throws ApplicationException;
    
    /**
     * Method tries to retrieve an ID of company that created the specified coupon by it's ID. 
     * @param coupon_id The Coupon ID.
     * @return Company ID if any.
     * @throws ApplicationException in case of error.
     */
    public Long getCompanyIdByCouponId(Long coupon_id) throws ApplicationException;
    
    /**
     * Method tries to retrieve list of coupons which end date is after the specified date.
     * @param date The date in SQL format Example: "2016-04-20".
     * @return Collection of Coupon that has endDate after <code>date</code>.
     * @throws ApplicationException in case of error.
     */
    public Collection<Coupon> getExpiredCoupons(java.sql.Date date) throws ApplicationException;

}
