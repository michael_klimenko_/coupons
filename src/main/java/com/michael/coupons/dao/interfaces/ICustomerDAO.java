package com.michael.coupons.dao.interfaces;

import com.michael.coupons.beans.Coupon;
import com.michael.coupons.beans.Customer;
import com.michael.coupons.entities.Category;
import com.michael.coupons.exceptions.ApplicationException;
import java.io.Serializable;
import java.util.Collection;

/**
 * ICustomerDAO interface is implemented by persistence mechanism class.
 * Includes declaration of necessary DAO actions for com.michael.coupons.beans.Customer model.
 * 
 * @author michael
 */
public interface ICustomerDAO extends Serializable {

    /**
     * Method tries to create a new customer.
     * @param customer The customer object populated with values.
     * @return The Customer object.
     * @throws ApplicationException in case of error.
     */
    public Customer createCustomer(Customer customer) throws ApplicationException;

    /**
     * Method tries to remove customer.
     * @param cust_id The Customer ID.
     * @return number of removed customer objects.
     * @throws ApplicationException in case of error.
     */
    public int removeCustomer(Long cust_id) throws ApplicationException;

    /**
     * Method tries to update customer.
     * @param customer The customer object with updated values.
     * @return The updated customer object.
     * @throws ApplicationException in case of error.
     */
    public Customer updateCustomer(Customer customer) throws ApplicationException;

    /**
     * Method tries to retrieve customer object by ID.
     * @param cust_id The Customer ID.
     * @return Customer object if exists.
     * @throws ApplicationException in case of error.
     */
    public Customer getCustomer(Long cust_id) throws ApplicationException;
    
    /**
     * Method tries to retrieve customer object by name.
     * @param name The Customer name.
     * @return The Customer Object if exists.
     * @throws ApplicationException in case of error.
     */
    public Customer getCustomerByName(String name) throws ApplicationException;

    /**
     * Method tries to retrieve all existing customers.
     * @return Collection of customer objects.
     * @throws ApplicationException in case of error.
     */
    public Collection<Customer> getAllCustomers() throws ApplicationException;

    /**
     * Method tries to retrieve coupon object by Customer ID and Coupon ID.
     * @param cust_id The Customer ID.
     * @param coupon_id The Coupon ID.
     * @return The Coupon object if exists.
     * @throws ApplicationException in case of error.
     */
    public Coupon getCoupon(Long cust_id, Long coupon_id) throws ApplicationException;
    
    /**
     * Method tries to retrieve all existing coupons of specified customer by customer ID.
     * @param cust_id The Customer ID.
     * @return Collection of coupon objects if any.
     * @throws ApplicationException in case of error.
     */
    public Collection<Coupon> getCoupons(Long cust_id) throws ApplicationException;
    
    /**
     * Method tries to retrieve all existing coupons of specified customer by customer ID filtered with Category type.
     * @param cust_id The Customer ID.
     * @param type the Category type.
     * @return Collection of coupon objects if any.
     * @throws ApplicationException in case of error.
     */
    public Collection<Coupon> getCoupons(Long cust_id, Category type) throws ApplicationException;
    
    /**
     * Method tries to retrieve all existing coupons of specified customer by customer ID up to specified price.
     * @param cust_id The Customer ID.
     * @param price price value.
     * @return Collection of coupon objects if any.
     * @throws ApplicationException in case of error.
     */
    public Collection<Coupon> getCoupons(Long cust_id, double price) throws ApplicationException;
    
    /**
     * Method tries to update reference for purchased coupons by Customer ID and Coupon ID.
     * @param cust_id The Customer ID.
     * @param coupon_id The Coupon ID.
     * @return number of inserted coupon references.
     * @throws ApplicationException in case of error.
     */
    public int couponPurchased(Long cust_id, Long coupon_id) throws ApplicationException;
    
    /**
     * Method tries to remove all coupon references for specified customer.
     * @param cust_id The customer ID.
     * @return number of removed references.
     * @throws ApplicationException in case of error.
     */
    public int removeAllCoupons(Long cust_id) throws ApplicationException;
    
    /**
     * Method tries to remove coupon reference by Coupon ID.
     * It used when company's coupons are removed and we need to cleanup coupon references.
     * All the purchased coupons are removed from reference table.
     * @param coupon_id The Coupon ID.
     * @return number of removed coupon references.
     * @throws ApplicationException in case of error.
     */
    public int removeCoupon(Long coupon_id) throws ApplicationException;

  /**
     * Method tries to verify provided credentials do match.
     * @param username The Username.
     * @param password The Password.
     * @return <code>true</code> in case of username/password do match;
     * <code>false</code> otherwise.
     * @throws ApplicationException in case of error.
     */
    public boolean login(String username, String password) throws ApplicationException;
}
