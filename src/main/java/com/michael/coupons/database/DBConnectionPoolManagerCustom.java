package com.michael.coupons.database;

import com.michael.coupons.database.IDBConnectionPoolManager;
import com.michael.coupons.exceptions.ApplicationException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import static com.michael.coupons.utils.ApplicationUtilities.CONFIG;
import static com.michael.coupons.utils.ApplicationUtilities.log;
import com.michael.coupons.utils.Exceptions;
import java.util.logging.Logger;

/**
 * Database connection pooling manager Custom implementation tested with
 * JavaDB/MySQL
 *
 * @author michael
 */
public class DBConnectionPoolManagerCustom implements IDBConnectionPoolManager {

    // =============== CONSTANTS ===================
    private static final long serialVersionUID = 1L;

    private static final String PROPERTY_URL = "database.dburl";
    private static final String PROPERTY_DRIVER = "database.dbdriver";
    private static final String PROPERTY_USERNAME = "database.username";
    private static final String PROPERTY_PASSWORD = "database.password";
    private static final String PROPERTY_POOL_SIZE = "database.pool_size";

    // =============== PROPERTIES ===================
    //The driver is automatically registered via the SPI and manual loading of the driver class is generally unnecessary.
    private final String dbDriver = CONFIG.getString(PROPERTY_DRIVER);
    private final String dbURL = CONFIG.getString(PROPERTY_URL);
    private final Integer poolSize = CONFIG.getInt(PROPERTY_POOL_SIZE);
    private final String username = CONFIG.getString(PROPERTY_USERNAME);
    private final String password = CONFIG.getString(PROPERTY_PASSWORD);

    private final List<Connection> connectionPool = new ArrayList();

    private static final DBConnectionPoolManagerCustom INSTANCE = new DBConnectionPoolManagerCustom();

// =============== CONSTRUCTORS ===================
    private DBConnectionPoolManagerCustom() {

        initializeConnectionPool();
    }

    // =============== ACTIONS ===================
    /**
     *
     * @return The DBConnectionPoolManager Instance
     */
    public static DBConnectionPoolManagerCustom getInstance() {
        return INSTANCE;
    }

    private void initializeConnectionPool() {
        while (!checkIfConnectionPoolIsFull()) {
            log("Connection Pool is NOT full. Proceeding with adding new connections", Level.FINEST, this.getClass().getName());
            try {
                //Adding new connection instance until the pool is full
                connectionPool.add(newConnectionForPool());
            } catch (Exception ex) {
                throw new ApplicationException(Exceptions.SQL, ex, 950, 500);
            }
        }
        log("Connection Pool is full.", Level.FINEST, this.getClass().getName());
    }

    //Creating a connection
    private Connection newConnectionForPool() {
        Connection connection = null;

        //Need to manually load driver for DerbyClient
        if (dbDriver.contains("derby")) {
            try {
                Class.forName(dbDriver);
            } catch (Exception ex) {
                throw new ApplicationException(Exceptions.DATABASE, ex, 822, 500);
            }
        }
        try {
            connection = DriverManager.getConnection(dbURL, username, password);
        } catch (Exception ex) {
            throw new ApplicationException(Exceptions.SQL, ex, 955, 500);
        }

        log("Connection: " + connection, Level.FINEST, this.getClass().getName());

        return connection;
    }

    private synchronized boolean checkIfConnectionPoolIsFull() {
        //Check if the pool size is reached
        return connectionPool.size() >= poolSize;
    }

    @Override
    public synchronized Connection getConnectionFromPool() {
        Connection connection = null;

        //Check if there is a connection available. There are times when all the connections in the pool may be used up
        if (connectionPool.size() > 0) {
            connection = (Connection) connectionPool.get(0);
            connectionPool.remove(0);
        }
        //Giving away the connection from the connection pool
        return connection;
    }

    @Override
    public synchronized void returnConnectionToPool(Connection connection) {
        //Adding the connection from the client back to the connection pool
        connectionPool.add(connection);
    }

    @Override
    public void closeAllConnections() {

        for (Connection connection : connectionPool) {
            try {
                connection.close();
            } catch (SQLException ex) {
                throw new ApplicationException(Exceptions.SQL, ex, 957, 500);
            }
        }

    }
}
