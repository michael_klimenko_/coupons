package com.michael.coupons.database;

import com.michael.coupons.database.IDBConnectionPoolManager;
import com.michael.coupons.exceptions.ApplicationException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import static com.michael.coupons.utils.ApplicationUtilities.CONFIG;
import static com.michael.coupons.utils.ApplicationUtilities.log;
import com.michael.coupons.utils.Exceptions;
import java.util.logging.Logger;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;

public class DBConnectionPoolManagerTomcat implements IDBConnectionPoolManager {

    // =============== CONSTANTS ===================
    private static final long serialVersionUID = 1L;

    private static final String PROPERTY_URL = "database.dburl";
    private static final String PROPERTY_DRIVER = "database.dbdriver";
    private static final String PROPERTY_USERNAME = "database.username";
    private static final String PROPERTY_PASSWORD = "database.password";

    // =============== PROPERTIES ===================
    private final String dbDriver = CONFIG.getString(PROPERTY_DRIVER);
    private final String dbURL = CONFIG.getString(PROPERTY_URL);
    private final String username = CONFIG.getString(PROPERTY_USERNAME);
    private final String password = CONFIG.getString(PROPERTY_PASSWORD);

    private static final DBConnectionPoolManagerTomcat INSTANCE = new DBConnectionPoolManagerTomcat();

    //Tomcat JDBC Connection Pooling
    private final PoolProperties p = new PoolProperties();
    private final DataSource datasource = new DataSource();

// =============== CONSTRUCTORS ===================
    private DBConnectionPoolManagerTomcat() {

        initializeConnectionPool();
    }

    // =============== ACTIONS ===================
    /**
     *
     * @return The DBConnectionPoolManager Instance
     */
    public static DBConnectionPoolManagerTomcat getInstance() {
        return INSTANCE;
    }

    private void initializeConnectionPool() {

        p.setUrl(dbURL);
        p.setDriverClassName(dbDriver);
        p.setUsername(username);
        p.setPassword(password);
        p.setJmxEnabled(true);
        p.setTestWhileIdle(false);
        p.setTestOnBorrow(true);
        p.setValidationQuery("SELECT 1");
        p.setTestOnReturn(false);
        p.setValidationInterval(30000);
        p.setTimeBetweenEvictionRunsMillis(30000);
        p.setMaxActive(100);
        p.setInitialSize(10);
        p.setMaxWait(10000);
        p.setRemoveAbandonedTimeout(60);
        p.setMinEvictableIdleTimeMillis(30000);
        p.setMinIdle(10);
        p.setLogAbandoned(true);
        p.setRemoveAbandoned(true);
        p.setJdbcInterceptors(
                "org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;"
                + "org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer");

        datasource.setPoolProperties(p);

        log("Connection Pool is initialized.", Level.FINEST, this.getClass().getName());
    }

    @Override
    public synchronized Connection getConnectionFromPool() {

        try {
            return datasource.getConnection();
        } catch (Exception ex) {
           throw new ApplicationException(Exceptions.SQL, ex, 951, 500);
        }

    }

    @Override
    public synchronized void returnConnectionToPool(Connection connection) {
        
        try {
            connection.close();
        } catch (SQLException ex) {
            throw new ApplicationException(Exceptions.SQL, ex, 952, 500);
        }
    }

    @Override
    public void closeAllConnections(){
        datasource.close(true);

    }
}
