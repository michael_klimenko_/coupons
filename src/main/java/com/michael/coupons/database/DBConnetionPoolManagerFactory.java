/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.michael.coupons.database;

import static com.michael.coupons.utils.ApplicationUtilities.CONFIG;

/**
 * Factory for database connections pool manager
 *
 * @author michael
 */
public class DBConnetionPoolManagerFactory {

    private static final String PROPERTY_POOL_TYPE  = "database.dbPoolMgrType";
    
    private static final String poolType  = CONFIG.getString(PROPERTY_POOL_TYPE);
    
    private static  IDBConnectionPoolManager dbConnectionPoolManager;

 
    
    public static IDBConnectionPoolManager getInstance() {

        switch (poolType) {
            case "custom":
                dbConnectionPoolManager = DBConnectionPoolManagerCustom.getInstance();
                break;
            case "tomcat":
                dbConnectionPoolManager = DBConnectionPoolManagerTomcat.getInstance();
                break;

        }
        return dbConnectionPoolManager;
    }
}
