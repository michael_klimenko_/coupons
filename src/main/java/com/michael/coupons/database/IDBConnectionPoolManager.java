package com.michael.coupons.database;

import java.sql.Connection;
import java.sql.SQLException;

public interface IDBConnectionPoolManager {

    public Connection getConnectionFromPool();

    public void returnConnectionToPool(Connection connection);

    public void closeAllConnections();
    
}
