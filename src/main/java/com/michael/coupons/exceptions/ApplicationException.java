package com.michael.coupons.exceptions;

import static com.michael.coupons.utils.ApplicationUtilities.log;
import com.michael.coupons.utils.Exceptions;

/**
 * This class represents a generic exception wrapper, for catching any type of
 * exceptions that may occur and normalize them.
 *
 * @author michael
 */
public class ApplicationException extends RuntimeException {

    // =============== CONSTANTS ===================
    private static final long serialVersionUID = 1L;

    // =============== PROPERTIES ===================
    /**
     * The variable representing exception type
     */
    private final Exceptions type;
    private final int intErrorCode;
    private final int httpStatusCode;

    // =============== CONSTRUCTORS ===================
    /**
     * Constructs an ApplicationException with the given detail message.
     *
     * @param message The detailed message of the exception
     */
    public ApplicationException(String message) {
        super(message);
        this.type = Exceptions.UNKNOWN;
        this.intErrorCode = 520;
        this.httpStatusCode = 520;
    }

    /**
     * Constructs an ApplicationException with the given error code and detail
     * message.
     *
     * @param errorCode
     * @param message The detailed message of the exception
     */
    public ApplicationException(int errorCode, String message) {
        super(message);
        this.type = Exceptions.UNKNOWN;
        this.intErrorCode = errorCode;
        this.httpStatusCode = 520;
    }

    /**
     * Constructs an ApplicationException with the given internal error code,
     * HTTP status code and detail message.
     *
     * @param errorCode
     * @param statusCode
     * @param message The detailed message of the exception
     */
    public ApplicationException(int errorCode, int statusCode, String message) {
        super(message);
        this.type = Exceptions.UNKNOWN;
        this.intErrorCode = errorCode;
        this.httpStatusCode = statusCode;
        log(type.name() + " : " + message);
    }

    /**
     * Constructs an ApplicationException with the given type and exception.
     *
     * @param type The exception type
     * @param expt The exception thrown
     */
    public ApplicationException(Exceptions type, Exception expt) {
        super(expt);
        this.type = type;
        this.intErrorCode = 999;
        this.httpStatusCode = 520;
        log(type.name() + " : " + expt.getMessage() + " " + expt.getStackTrace().toString());
    }

    /**
     * Constructs an ApplicationException with the given type, int error code,
     * HTTP status and exception.
     *
     * @param type The exception type
     * @param expt The exception thrown
     */
    public ApplicationException(Exceptions type, Exception expt, int errorCode, int statusCode) {
        super(expt);
        this.type = type;
        this.intErrorCode = errorCode;
        this.httpStatusCode = statusCode;
        log(type.name() + " : " + expt.getMessage() + " " + expt.getStackTrace().toString());
    }

    /**
     * Constructs an ApplicationException with exception thrown, type is
     * unknown.
     *
     * @param expt The exception thrown
     */
    public ApplicationException(Exception expt) {
        super(expt);
        this.type = Exceptions.UNKNOWN;
        this.intErrorCode = 999;
        this.httpStatusCode = 520;
        log(type.name() + " : " + expt.getMessage() + " " + expt.getStackTrace().toString());
    }

    /**
     * Constructs an ApplicationException with type, exception is unknown.
     *
     * @param type The exception type
     */
    public ApplicationException(Exceptions type) {
        super(type.name());
        this.type = type;
        this.intErrorCode = 999;
        this.httpStatusCode = 520;
        log(type.name());
    }

    /**
     * Constructs an ApplicationException with type, exception is unknown.
     *
     * @param type The exception type.
     * @param message Custom message.
     */
    public ApplicationException(Exceptions type, String message) {
        super(message);
        this.type = type;
        this.intErrorCode = 999;
        this.httpStatusCode = 520;
        log(type.name() + " : " + message);
    }

    /**
     * Constructs an ApplicationException with type, exception is unknown.
     *
     * @param type The exception type.
     * @param message Custom message.
     */
    public ApplicationException(Exceptions type, int errorCode, String message) {
        super(message);
        this.type = type;
        this.intErrorCode = errorCode;
        this.httpStatusCode = 520;
        log(type.name() + " : " + message);
    }

    /*
    @Override
    public String toString() {
        
        return "ApplicationException{ "
                + "Type: [" + exception.toString() 
                + "] Cause: [" + ex.getCause() 
                + "] Message: [" + ex.getMessage() 
                + "] StackTrace: [" + ex.getStackTrace() 
                + "]}";
        
    }
     */
    /**
     * Constructs an ApplicationException with type, internal error, HTTP status
     * code and message.
     *
     * @param type The exception type.
     * @param errorCode
     * @param statusCode
     * @param message Custom message.
     */
    public ApplicationException(Exceptions type, int errorCode, int statusCode, String message) {
        super(message);
        this.type = type;
        this.intErrorCode = errorCode;
        this.httpStatusCode = statusCode;
        log(type.name() + " : " + message);
    }

    /*
    @Override
    public String toString() {
        
        return "ApplicationException{ "
                + "Type: [" + exception.toString() 
                + "] Cause: [" + ex.getCause() 
                + "] Message: [" + ex.getMessage() 
                + "] StackTrace: [" + ex.getStackTrace() 
                + "]}";
        
    }
     */
    public int getErrorCode() {
        return this.intErrorCode;
    }

    public Exceptions getType() {
        return type;
    }

    public int getHttpStatus() {
        return httpStatusCode;
    }

}
