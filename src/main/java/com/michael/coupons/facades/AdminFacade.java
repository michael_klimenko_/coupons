package com.michael.coupons.facades;

import com.michael.coupon.web.utils.cgrates.Account;
import com.michael.coupon.web.utils.cgrates.AttrGetAccount;
import com.michael.coupon.web.utils.cgrates.AttrGetAccounts;
import com.michael.coupons.beans.Company;
import com.michael.coupons.beans.Coupon;
import com.michael.coupons.beans.Customer;
import com.michael.coupons.dao.AccountJSONRPCDAO;
import com.michael.coupons.dao.CompanyDBDAO;
import com.michael.coupons.dao.CouponDBDAO;
import com.michael.coupons.dao.CustomerDBDAO;
import com.michael.coupons.exceptions.ApplicationException;
import com.michael.coupons.utils.ApplicationUtilities;
import com.michael.coupons.utils.ClientType;
import com.michael.coupons.utils.Exceptions;
import java.util.Collection;
import java.util.List;

/**
 * AdminFacade represents a wrapper for AdminDBDAO class and offers management functionality for the Coupon System Administrator. Implements
 * ICouponClientFacade for using shared login function.
 *
 * @author michael
 */
public class AdminFacade implements ICouponClientFacade {

    // ======================CONSTANTS========================
    private static final long serialVersionUID = 1L;

    // =============== PROPERTIES ===================
    // get DBDAO instances 
    private final CompanyDBDAO companyDBDAO = CompanyDBDAO.getInstance();
    private final CustomerDBDAO customerDBDAO = CustomerDBDAO.getInstance();
    private final CouponDBDAO couponDBDAO = CouponDBDAO.getInstance();
    private final AccountJSONRPCDAO accountJSONRPCDAO = AccountJSONRPCDAO.getInstance();

    // ==============SINGLETONE======================
    private static final AdminFacade INSTANCE = new AdminFacade();

    private AdminFacade() {
    }

    public static AdminFacade getInstance() {
        return INSTANCE;
    }

    // =============== METHODS ===================
    //  Companies' section 
    /**
     * Method tries to creates a new company.
     *
     * @param company Company object populated with values is expected.
     * @return Company created.
     * @throws ApplicationException in case same company name exists or SQLException.
     */
    public Company createCompany(Company company) throws ApplicationException {

        // check if company with the same name exists
        if (companyDBDAO.getCompanyByName(company.getComp_name()) != null) {

            throw new ApplicationException(Exceptions.COMPANY, "Company with the same name (" + company.getComp_name() + ") already exists.");
        } else {
            //create new company
            return companyDBDAO.createCompany(company);
        }

    }

    /**
     * Method tries to remove company. If the company has created coupons, it will be removed from coupon table and from all the customers that
     * purchased it
     *
     * @param comp_id existing company id is expected.
     * @return Company removed.
     * @throws ApplicationException in case company doesn't exist or SQLException.
     */
    public int removeCompany(Long comp_id) throws ApplicationException {

        //get company by id
        Company company = companyDBDAO.getCompany(comp_id);

        if (company != null) {

            // check if company has any created coupons
            if (!company.getCoupons().isEmpty()) {

                for (Coupon coupon : company.getCoupons()) {

                    //remove coupon purchased by customers
                    customerDBDAO.removeCoupon(coupon.getId());

                    //remove coupon itself
                    couponDBDAO.removeCoupon(coupon.getId());
                }
                //remove all coupons created by this company
                companyDBDAO.removeAllCoupons(comp_id);
            }

            //remove company
            return companyDBDAO.removeCompany(comp_id);

        } else {

            throw new ApplicationException(Exceptions.COMPANY, 884, 404, "Company with id: " + comp_id + " doesn't exist.");
        }

    }

    /**
     * Method tries to update existing company attributes. Only <code>password</code> and <code>email</code> will be updated. <code>name</code> will
     * be ignored and left intact.
     *
     * @param company Company object with updated attributes.
     * @return Company updated.
     * @throws ApplicationException in case company doesn't exist or SQLException.
     */
    public Company updateCompany(Company company) throws ApplicationException {

        return companyDBDAO.updateCompany(company);

    }

    /**
     * Method tries to retrieve company by id from the database.
     *
     * @param comp_id The company ID.
     * @return Company object.
     * @throws ApplicationException in case company doesn't exist or SQLException.
     */
    public Company getCompany(Long comp_id) throws ApplicationException {
        Company company = companyDBDAO.getCompany(comp_id);

        if (company == null) {

            throw new ApplicationException(Exceptions.OBJECT_DOESNT_EXIST, 860, 401, "Company with id " + comp_id + " doesn't exist.");

        }
        return company;
    }

    /**
     * Method tries to retrieve all Companies from the database.
     *
     * @return Collection of Company objects.
     * @throws ApplicationException in case of DB error.
     */
    public Collection<Company> getAllCompanies() throws ApplicationException {

        Collection<Company> companies = companyDBDAO.getAllCompanies();

        if (companies.isEmpty()) {

            throw new ApplicationException(Exceptions.OBJECT_DOESNT_EXIST, 880, 404, "No companies exist");

        }

        return companies;
    }

    // Customers' section 
    /**
     * Method tries to create a new Customer.
     *
     * @param customer Customer object populated with values is expected.
     * @return Customer created.
     * @throws ApplicationException if customer with the same name exists or DB error.
     */
    public Customer createCustomer(Customer customer) throws ApplicationException {

        // check if the the same name customer already exists
        if (customerDBDAO.getCustomerByName(customer.getCust_name()) != null) {
            throw new ApplicationException(Exceptions.CUSTOMER, 888, 520, "Customer with the same name (" + customer.getCust_name() + ") already exists.");
        } else {
            return customerDBDAO.createCustomer(customer);
        }
    }

    /**
     * Method tries to remove existing Customer. If the customer has purchased coupons, it will be removed as well.
     *
     * @param cust_id The existing customer id is expected
     * @return Customer removed.
     * @throws ApplicationException if the customer doesn't exist or DB error.
     */
    public int removeCustomer(Long cust_id) throws ApplicationException {

        // retrieve customer by id
        Customer customer = customerDBDAO.getCustomer(cust_id);

        if (customer != null) {

            //check if customer has any purchased coupons
            if (!customer.getCoupons().isEmpty()) {

                //remove coupons
                customerDBDAO.removeAllCoupons(customer.getId());

            }
            //remove customer
            return customerDBDAO.removeCustomer(cust_id);

        } else {

            throw new ApplicationException(Exceptions.CUSTOMER, "Customer with id: " + cust_id + " doesn't exist.");

        }

    }

    /**
     * Method tries to update existing Customer in database. Only password can be changed. name will be ignored and left intact.
     *
     * @param customer Customer object with updated attributes is expected.
     * @return Customer updated.
     * @throws ApplicationException in case Customer doesn't exist or DB error.
     */
    public Customer updateCustomer(Customer customer) throws ApplicationException {

        return customerDBDAO.updateCustomer(customer);
    }

    /**
     * Method tries to retrieve Customer object by Id from the database.
     *
     * @param cust_id Customer ID.
     * @return Customer object.
     * @throws ApplicationException if Customer doesn't exist or DB error.
     */
    public Customer getCustomer(Long cust_id) throws ApplicationException {

        return customerDBDAO.getCustomer(cust_id);
    }

    /**
     * Method tries to retrieve all Customer objects from the database.
     *
     * @return Collection of Customer objects or <code>null</code> if none exist.
     * @throws ApplicationException if DB error
     */
    public Collection<Customer> getAllCustomers() throws ApplicationException {

        return customerDBDAO.getAllCustomers();
    }

    /**
     * Method tries to retrieve all Coupon objects from the database.
     *
     * @return Collection of Coupon objects or <code>null</code> if none exist.
     * @throws ApplicationException if none exists or DB error
     */
    public Collection<Coupon> getAllCoupons() throws ApplicationException {

        return couponDBDAO.getAllCoupons();

    }

    /**
     * Method tries to retrieve all IDs of the customers that have purchased specific Coupon by couponID. Used in JUnit tests
     *
     * @param coupon_id The Coupon ID
     * @return Collection of Customer objects or <code>null</code> if none exist.
     * @throws ApplicationException if DB error.
     */
    public List<Long> getCustomersByCouponId(Long coupon_id) throws ApplicationException {
        return couponDBDAO.getCustomersIdsByCouponId(coupon_id);
    }

    /**
     * Method tries to retrieve ID of the company that created a Coupon by couponID. Used in JUnit tests
     *
     * @param couponId the id of coupon
     * @return CompanyId
     * @throws ApplicationException if DB error
     */
    public Long getCompanyIdByCouponId(Long couponId) throws ApplicationException {
        return couponDBDAO.getCompanyIdByCouponId(couponId);
    }

    /**
     * Method allows company manager to retrieve coupon by coupon ID.
     *
     * @param coupon_id The Coupon ID.
     * @return Coupon object if exists.
     * @throws ApplicationException in case of error.
     */
    public Coupon getCoupon(Long coupon_id) throws ApplicationException {

        return couponDBDAO.getCoupon(coupon_id);

    }

    public Account[] getAccounts() {

        return accountJSONRPCDAO.getAccounts(new AttrGetAccounts());

    }

    public Account getAccount(String msisdn) {

        return accountJSONRPCDAO.getAccount(new AttrGetAccount(msisdn));

    }

    @Override
    public boolean login(String username, String password, ClientType clientType) throws ApplicationException {

        return "admin".equals(username) && "1234".equals(password) && clientType.equals(ClientType.ADMIN);

    }

}
