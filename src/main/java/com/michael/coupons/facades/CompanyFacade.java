package com.michael.coupons.facades;

import com.michael.coupons.beans.Company;
import com.michael.coupons.beans.Coupon;
import com.michael.coupons.dao.CompanyDBDAO;
import com.michael.coupons.dao.CouponDBDAO;
import com.michael.coupons.dao.CustomerDBDAO;
import com.michael.coupons.entities.Category;
import com.michael.coupons.exceptions.ApplicationException;
import com.michael.coupons.utils.ClientType;
import com.michael.coupons.utils.Exceptions;
import java.text.SimpleDateFormat;
import java.util.Collection;

/**
 * CompanyFacade represents a wrapper for CompanyDBDAO class and offers management functionality for the Coupon System Company user.
 * Implements ICouponClientFacade for using shared login function.
 * 
 * @author michael
 */
public class CompanyFacade implements ICouponClientFacade {
    
    // ======================CONSTANTS========================
    private static final long serialVersionUID = 1L;
    
    // =============== PROPERTIES ===================
    
    // get DBDAO instances 
    private final CompanyDBDAO companyDBDAO = CompanyDBDAO.getInstance();
    private final CustomerDBDAO customerDBDAO = CustomerDBDAO.getInstance();
    private final CouponDBDAO couponDBDAO = CouponDBDAO.getInstance();

     // ==============SINGLETONE======================
    private static final CompanyFacade INSTANCE = new CompanyFacade();
    
    private CompanyFacade () {}
    
    public static CompanyFacade getInstance() {
        return INSTANCE;
    }
    
    // =============== METHODS ===================
    
    /**
     * Method allows company manager to create a new coupon.
     * Coupon reference is created in company_coupons as well.
     * @param comp_id Company ID
     * @param coupon Coupon object populated with values is expected.
     * @return Coupon created.
     * @throws ApplicationException in case coupon with the same title exist.
     */
    public Coupon createCoupon(Long comp_id, Coupon coupon) throws ApplicationException {

    if (couponDBDAO.getCouponByTitle(coupon.getTitle()) != null) {
    
        throw new ApplicationException(Exceptions.COUPON, 803, 520, "Coupon with the same title already exists.");
        
    } else {
        
        //create new coupon
        Coupon newCoupon = couponDBDAO.createCoupon(coupon);
        //update reference to company
        companyDBDAO.couponCreated(comp_id, newCoupon.getId());
        
        return newCoupon;     
        }
    
    }

    /**
     * Method allows company manager to remove coupon.
     * Coupon reference is removed from company_coupons table as well.
     * Purchased coupon references are removed from customer_coupons table as well.
     * @param comp_id Company ID
     * @param coupon_id The Coupon ID.
     * @return number of removed coupons.
     * @throws ApplicationException in case of error.
     */
    public int removeCoupon(Long comp_id, Long coupon_id) throws ApplicationException {

       
        
        if (companyDBDAO.getCompany(comp_id).getCoupons().contains(this.getCoupon(coupon_id))) {
                       
             int result = 0;
             
            //remove purchased coupon references from customer_coupons
            customerDBDAO.removeCoupon(coupon_id);

            //remove coupon reference from company_coupons table
            companyDBDAO.removeCoupon(coupon_id);

            //remove coupon itself from coupons table
            result = couponDBDAO.removeCoupon(coupon_id);

            return result;
             
        } else {
        
             throw new ApplicationException(Exceptions.COUPON, 803, 520, "Cannon delete coupon with id :"+coupon_id+", because it doesn't belong to company with id: "+comp_id);
        }
        
       
        
    }

    /**
     * Method allows company manager to update coupon details.
     * Attributes allowed to edit are : 
     * message, price, amount, the date of start and end, image and category.
     * @param comp_id Company ID
     * @param coupon The Coupon object populated with updated values.
     * @return Coupon updated
     * @throws ApplicationException in case of error
     */
    public Coupon updateCoupon(Long comp_id, Coupon coupon) throws ApplicationException {

        
         if (companyDBDAO.getCompany(comp_id).getCoupons().contains(getCoupon(coupon.getId()))) {
        
        return couponDBDAO.updateCoupon(coupon);
        
        } else {
        
             throw new ApplicationException(Exceptions.COUPON, 803, 520, "Cannon update coupon with id :"+coupon.getId()+", because it doesn't belong to company with id: "+comp_id);
        }

    }

    /**
     * Method allows company manager to retrieve coupon by coupon ID.
     * @param coupon_id The Coupon ID.
     * @return Coupon object if exists.
     * @throws ApplicationException in case of error.
     */
    public Coupon getCoupon(Long coupon_id) throws ApplicationException {

        return couponDBDAO.getCoupon(coupon_id);

    }

    /**
     * Method allows company manager to retrieve all company coupons.
     * @param comp_id The Company ID.
     * @return Collection of coupon objects.
     * @throws ApplicationException in case of error.
     */
    public Collection<Coupon> getAllCoupons(Long comp_id) throws ApplicationException {

        return companyDBDAO.getCoupons(comp_id);

    }
    
    /**
     * Method allows company manager to retrieve all company coupons filtered with category type.
     * @param comp_id The Company ID.
     * @param type Category type.
     * @return Collection of coupon objects.
     * @throws ApplicationException in case of error.
     */
    public Collection<Coupon> getCouponByType(Long comp_id, Category type) throws ApplicationException {

        return companyDBDAO.getCoupons(comp_id,type);

    }
    
    /**
     * Method allows company manager to retrieve all company coupons up to specified price.
     * @param comp_id The Company ID.
     * @param price The price value.
     * @return Collection of coupon objects.
     * @throws ApplicationException in case of error.
     */
    public Collection<Coupon> getCouponByPrice(Long comp_id, double price) throws ApplicationException {

        return companyDBDAO.getCoupons(comp_id,price);

    }

    /**
     * Method allows company manager to retrieve all company coupons up to specified date.
     * @param comp_id The Company ID.
     * @param date The date.
     * @return Collection of coupon objects.
     * @throws ApplicationException in case of error.
     */
    public Collection<Coupon> getCouponByDate(Long comp_id, String date) throws ApplicationException {

        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date parsed = format.parse(date);
            java.sql.Date sqldate = new java.sql.Date(parsed.getTime());
            
            return companyDBDAO.getCoupons(comp_id,sqldate);
            
        } catch (Exception ex) {
            throw new ApplicationException(Exceptions.COUPON, ex, 455, 412);        
        }

    }

    /**
     * Method allows company manager to retrieve company object.
     * @param comp_id The Company ID.
     * @return Company object.
     * @throws ApplicationException in case of error.
     */
    public Company getCompany(Long comp_id) throws ApplicationException {

        return companyDBDAO.getCompany(comp_id);
    }
    
    /** 
     * Method allows to get company object by name.
     * @param comp_name
     * @return Company object.
     * @throws ApplicationException in case of error.
     */
    //TODO: Reduce from whole company object to Long id only
    public Company getCompanyByName(String comp_name) throws ApplicationException {

        return companyDBDAO.getCompanyByName(comp_name);
    }

    /**
     * Method allows company manager to retrieve all existing companies.
     * @return Collection of company objects.
     * @throws ApplicationException in case of error.
     */
    public Collection<Company> getAllCompanies() throws ApplicationException {

        return companyDBDAO.getAllCompanies();
    }

    /**
     * Method allows company manager to create a new company.
     * company with the same name are not allowed.
     * @param company The Company Object.
     * @return Created Company Object.
     * @throws ApplicationException in case of error.
     */
    public Company createCompany(Company company) throws ApplicationException {

        if (companyDBDAO.getCompanyByName(company.getComp_name()) == null) {
            return companyDBDAO.createCompany(company);
        } else {
            throw new ApplicationException(Exceptions.COMPANY, "Company with the same name exists.");
        }
    }
    
    /**
     * Method allows company manager to remove a company.
     * Clears both customer_coupon and company_coupons references.
     * Removes coupons created by this company.
     * @param comp_id The Company ID.
     * @return number of removed companies.
     * @throws ApplicationException in case of error.
     */
    public int deleteCompany(Long comp_id) throws ApplicationException {

        //get company by id
        Company company = companyDBDAO.getCompany(comp_id);

        if (company != null) {

            // check if company has any created coupons
            if (!company.getCoupons().isEmpty()) {

                for (Coupon coupon : company.getCoupons()) {

                    //remove coupon purchased by customers
                    customerDBDAO.removeCoupon(coupon.getId());

                    //remove coupon itself
                    couponDBDAO.removeCoupon(coupon.getId());
                }
                //remove all coupons created by this company
                companyDBDAO.removeAllCoupons(comp_id);
            }

            //remove company
            return companyDBDAO.removeCompany(comp_id);

        } else {

            throw new ApplicationException(Exceptions.COMPANY, "Company doesn't exist.");
        }

    }

    /**
     * Method allows company manager to update company details.
     * Attributes allowed to edit are:
     * Email and Password.
     * @param company Company object populated with updated values.
     * @return Company updated
     * @throws ApplicationException in case of error.
     */
    public Company updateCompany(Company company) throws ApplicationException {

            return companyDBDAO.updateCompany(company);
    }

    @Override
     public boolean login(String username, String password, ClientType clientType) throws ApplicationException {
        if (clientType.equals(ClientType.COMPANY)) {
            return companyDBDAO.login(username, password);
        } else {
            throw new ApplicationException(Exceptions.INVALID_LOGIN_TYPE);
        }
     }
}
