package com.michael.coupons.facades;

import com.michael.coupons.beans.Customer;
import com.michael.coupons.beans.Coupon;
import com.michael.coupons.dao.CouponDBDAO;
import com.michael.coupons.dao.CustomerDBDAO;
import com.michael.coupons.entities.Category;
import com.michael.coupons.exceptions.ApplicationException;
import com.michael.coupons.utils.ClientType;
import com.michael.coupons.utils.CouponUtils;
import com.michael.coupons.utils.Exceptions;
import java.util.Collection;

/**
 * This class provides basic functions for Customer to check his coupon purchase
 * history and to buy valid coupons.
 *
 * @author michael
 */
public class CustomerFacade implements ICouponClientFacade {
    
    // ======================CONSTANTS========================
    private static final long serialVersionUID = 1L;
    
    // ======================PARAMETERS========================
    private final CustomerDBDAO customerDBDAO = CustomerDBDAO.getInstance();
    private final CouponDBDAO couponDBDAO = CouponDBDAO.getInstance();

     // ==============SINGLETONE======================
    private static final CustomerFacade INSTANCE = new CustomerFacade();
    
    private CustomerFacade () {}
    
    public static CustomerFacade getInstance() {
        return INSTANCE;
    }

    // =============== METHODS ===================
    
    /**
     * This method offers a client option to buy a coupon rules : * only 1
     * coupon per client is allowed * check if coupon is not expired * check if
     * coupon is in stock
     *
     * @param cust_id The Customer object id
     * @param coupon_id The Coupon object id
     * @return The purchased coupon
     * @throws ApplicationException in case coupon isn't valid or DB error
     */
    public Coupon purchaseCoupon(Long cust_id, Long coupon_id) throws ApplicationException {

        Coupon coupon = couponDBDAO.getCoupon(coupon_id);
        Customer customer = customerDBDAO.getCustomer(cust_id);
        
        if (CouponUtils.isInStock(coupon)) {

            if (!CouponUtils.isExpired(coupon)) {

                if (!customer.getCoupons().contains(coupon)) {

                    coupon.setAmount(coupon.getAmount() - 1);
                    couponDBDAO.updateCoupon(coupon);
                    customerDBDAO.couponPurchased(customer.getId(), coupon.getId());

                } else {

                    throw new ApplicationException(Exceptions.COUPON, 607, 520, "Coupon is already purchased");
                }
            } else {

                throw new ApplicationException(Exceptions.COUPON, 606, 520, "Coupon is expired");
            }
        } else {

            throw new ApplicationException(Exceptions.COUPON, 605, 520, "Coupon is not in stock");
        }

        return coupon;
    }
    
    /**
     * This method offers a client option to retrieve all purchased coupons by customer ID.
     * @param cust_id The Customer ID.
     * @return Collection of coupon objects if any.
     * @throws ApplicationException in case of error.
     */
    public Collection<Coupon> getAllPurchasedCoupons(Long cust_id) throws ApplicationException {

        return customerDBDAO.getCoupons(cust_id);

    }

    /**
     * This method offers a client option to retrieve all purchased coupons by customer ID filtered with category type.
     * @param cust_id The Customer ID.
     * @param type The category type.
     * @return Collection of coupon objects if any.
     * @throws ApplicationException in case of error.
     */
    public Collection<Coupon> getAllPurchasedCouponsByType(Long cust_id, Category type) throws ApplicationException {

        return customerDBDAO.getCoupons(cust_id, type);
    }

    /**
     * This method offers a client option to retrieve all purchased coupons by customer ID up to specified price.
     * @param cust_id The Customer ID.
     * @param price price value.
     * @return Collection of coupon objects if any.
     * @throws ApplicationException in case of error.
     */
    public Collection<Coupon> getAllPurchasedCouponsByPrice(Long cust_id, double price) throws ApplicationException {

        return customerDBDAO.getCoupons(cust_id, price);
    }
    
     /**
     * Method allows company manager to retrieve coupon by coupon ID.
     * @param coupon_id The Coupon ID.
     * @return Coupon object if exists.
     * @throws ApplicationException in case of error.
     */
    public Coupon getCoupon(Long coupon_id) throws ApplicationException {

        return couponDBDAO.getCoupon(coupon_id);

    }
    
    /**
     * Method tries to retrieve all Coupon objects from the database.
     *
     * @return Collection of Coupon objects or <code>null</code> if none
     * exist.
     * @throws ApplicationException if none exists or DB error
     */
    public Collection<Coupon> getAllCoupons() throws ApplicationException {

        return couponDBDAO.getAllCoupons();
        
    }
    /**
     * Method tries to retrieve Customer object by Name from the database.
     *
     * @param cust_name customer name
     * @return Customer object.
     * @throws ApplicationException if Customer doesn't exist or DB error.
     */
    //TODO: Reduce from whole company object to Long id only
    public Customer getCustomerByName(String cust_name) throws ApplicationException {

        return customerDBDAO.getCustomerByName(cust_name);
    }
    
    

    @Override
    public boolean login(String username, String password, ClientType clientType) throws ApplicationException {
        if (clientType.equals(ClientType.CUSTOMER)) {
            return customerDBDAO.login(username, password);
        } else {
            throw new ApplicationException(Exceptions.INVALID_LOGIN_TYPE);
        }
    }
}
