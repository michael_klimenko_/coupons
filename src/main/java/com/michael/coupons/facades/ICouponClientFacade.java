
package com.michael.coupons.facades;

import com.michael.coupons.exceptions.ApplicationException;
import com.michael.coupons.utils.ClientType;
import java.io.Serializable;

/**
 * ICouponClientFacade interface is used to provide shared login procedure for gaining suitable facade class.
 * @author michael
 */
public interface ICouponClientFacade extends Serializable {
    
    
   /**
     * Shared Method for checking provided credential
     * @param username The Username.
     * @param password The Password.
     * @param clientType client type can be: ClientType.ADMIN, ClientType.COMPANY or ClientType.CUSTOMER
     * @return <code>true</code> in case of username/password do match;
     * <code>false</code> otherwise.
     * @throws ApplicationException in case of error.
     */
    public boolean login (String username, String password, ClientType clientType) throws ApplicationException ;
    
}
