/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.michael.coupons.utils;


import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.logging.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * ApplicationUtilities class is used by Coupon project for various operations
 * amongst other classes.
 *
 * Read CONFIG variables Print exceptions etc...
 */
public class ApplicationUtilities {

    public static final Config CONFIG = ConfigFactory.load();
    private static final Logger slf4jLogger = LoggerFactory.getLogger(ApplicationUtilities.class);

    public static List<Column> getColumnsList(String where) {

        List<Column> columnsMap = new ArrayList<>();
        List<? extends Config> columnsConfig = CONFIG.getConfigList(where);
        columnsConfig.stream().forEach((col) -> {
            columnsMap.add(new Column(col));
        });

        return columnsMap;
    }

    public static void printSQLException(SQLException e) {
        System.err.print(e.getMessage());
    }

    public static void print(NullPointerException e) {
        System.err.print(e.getMessage());
    }

    public static void log(String message, Level loglevel, String loggerName) {
        //Handler logFormatter;
        if (loglevel == Level.FINEST) {
            slf4jLogger.debug(message);
        }
        if (loglevel == Level.INFO) {
            slf4jLogger.info(message);
        }
        if (loglevel == Level.WARNING) {
            slf4jLogger.warn(message);
        }
        if (loglevel == Level.SEVERE) {
            slf4jLogger.error(message);
        }
        //Logger.getLogger(loggerName).log(loglevel, message);
        //LOG.log(loglevel, "{0}|{1}|{2}", new Object[]{getFormatedDate(), loglevel, message});
        //System.out.println(getFormatedDate()+"|"+loglevel+"|"+message);
        //Logger.getLogger(Tester.class.getName()).log(Level.SEVERE, "INFO", ex);
        // %1$tY-%1$tm-%1$td %1$tH:%1$tM:%1$tS %4$-6s %2$s %5$s%6$s%n
    }

    public static void log(String message, String loggerName) {
        log(message, Level.INFO, loggerName);
        //LOG.log(loglevel, "{0}|{1}|{2}", new Object[]{getFormatedDate(), loglevel, message});
        //System.out.println(getFormatedDate()+"|"+loglevel+"|"+message);
        //Logger.getLogger(Tester.class.getName()).log(Level.SEVERE, "INFO", ex);

    }

    public static void log(String message, Level loglevel) {
        log(message, loglevel, ApplicationUtilities.class.getName());
        //LOG.log(loglevel, "{0}|{1}|{2}", new Object[]{getFormatedDate(), loglevel, message});
        //System.out.println(getFormatedDate()+"|"+loglevel+"|"+message);
        //Logger.getLogger(Tester.class.getName()).log(Level.SEVERE, "INFO", ex);

    }

    public static void log(String message) {
        log(message, Level.INFO);

    }

    public static String getFormatedDate() {

        Locale currentLocale = new Locale("en_US");
        /* 
        //Date
        DateFormat dateFormatter = DateFormat.getDateInstance(DateFormat.SHORT,currentLocale);
        String formatedDate = dateFormatter.format(new Date(timestamp));
        //Time
        DateFormat timeFormatter = DateFormat.getTimeInstance(DateFormat.MEDIUM,currentLocale);
        String formatedTime = timeFormatter.format(new Date(timestamp));
        // Date & Time
        DateFormat datetimeFormatter = DateFormat.getDateTimeInstance(DateFormat.SHORT,DateFormat.MEDIUM,currentLocale);
        String formatedDatetime = datetimeFormatter.format(new Date(timestamp));
         */

        //custom date time format
        String pattern = "YYYY-MM-dd HH:mm:ss.SSS";
        SimpleDateFormat formatter = new SimpleDateFormat(pattern, currentLocale);

        String customFormatedDatetime = formatter.format(new Date());

        return customFormatedDatetime;
    }
    
    public static String  getId() {
    
           return UUID.randomUUID().toString().replace("-", "");
    
    }
}
