package com.michael.coupons.utils;

/* AutoLoadJdbcDriver.java
 - Copyright (c) 2015, HerongYang.com, All Rights Reserved.
 */
import java.sql.*;
import java.util.*;
public class AutoLoadJdbcDriver {
  public static void main(String args[]) {
    Connection con = null;
    try {
      //listDrivers();

// Find the driver for a given URL
      Driver driverClass = (Driver) DriverManager.getDriver(
        "jdbc:mysql://10.192.210.100:3306");
    
      System.out.println("  Detected:  "+driverClass.getClass().getName());
 
      listDrivers();
    } catch (Exception e) {
      System.err.println("Exception: "+e.getMessage());
    }
  }
  private static void listDrivers() {
    Enumeration driverList = DriverManager.getDrivers();
    System.out.println("\nList of drivers:");
    while (driverList.hasMoreElements()) {
      Driver driverClass = (Driver) driverList.nextElement();
      System.out.println("   "+driverClass.getClass().getName());
    }
  }
}