/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.michael.coupons.entities;

/**
 *
 * @author michael
 */
public enum Category {
    
        RESTAURANTS(0),
    	ELECTRICITY(1),
    	FOOD(2),
    	HEALTH(3),
    	SPORTS(4),
        CAMPING(5),
        TRAVELLING(6),
        ENTERTAINMENT(7);

    	
    	private Category(int t){
    		value=t;
    	}

    	//Default ENTERTAINMENT
    	private int value = 7;

    	public int getValue(){
    		return value;
    	}
         public static Category fromInt(int i) {
        for (Category b : Category .values()) {
            if (b.getValue() == i) { return b; }
        }
        return null;
    }
}
