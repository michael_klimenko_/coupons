package com.michael.coupons.utils;

/**
 * Enum class for representing client types.
 * @author michael
 */
public enum ClientType {
    ADMIN,
    COMPANY,
    CUSTOMER
}
