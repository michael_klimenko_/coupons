package com.michael.coupons.utils;

/*
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */

import com.michael.coupon.web.utils.cgrates.Account;
import com.michael.coupon.web.utils.cgrates.AttrGetAccount;
import com.michael.coupon.web.utils.cgrates.Balance;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.commons.codec.Charsets;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

/**
 * This example demonstrates the use of the {@link ResponseHandler} to simplify the process of processing the HTTP response and releasing associated
 * resources.
 */
public class ClientWithResponseHandler {

    public final static void main(String[] args) throws Exception {

        CloseableHttpClient httpclient = HttpClients.createDefault();
        AttrGetAccount attrGetAccount = new AttrGetAccount("972585577773");

        try {
            HttpPost post = new HttpPost("http://10.224.228.186:2080/jsonrpc");
            post.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
            post.setEntity(new StringEntity(attrGetAccount.createJsonRpc("test", "ApierV2.GetAccount").toString(), Charsets.UTF_8));

            System.out.println("Executing request " + post.getRequestLine());

            // Create a custom response handler
            ResponseHandler<String> responseHandler = new ResponseHandler<String>() {

                @Override
                public String handleResponse(
                        final HttpResponse response) throws ClientProtocolException, IOException {
                    int status = response.getStatusLine().getStatusCode();
                    if (status >= 200 && status < 300) {
                        HttpEntity entity = response.getEntity();
                        JSONObject result = new JSONObject(entity != null ? EntityUtils.toString(entity) : null);
                        System.out.println("response handler: " + result.toString());
                        return result.toString();
                    } else {
                        throw new ClientProtocolException("Unexpected response status: " + status);
                    }
                }

            };
            Account account = new Account(new JSONObject(httpclient.execute(post, responseHandler)).getJSONObject("result"));
            System.out.println("----------------------------------------");
            System.out.println("response: " + account.createJsonRpc("test", "foo"));

           
            account.getBalanceMap().keySet().stream().map((key) -> {
                System.out.println("account balance type: " + key);
                return key;
            }).forEach( (key) -> {
                 
            ArrayList<Balance> balances = account.getBalanceMap().get(key);
            for (Balance balance : balances) {
                    System.out.println("account balances: " + balance.getID() + " " + balance.getValue() + " " + balance.getExpirationDate());
            }
                
            });
             
             
             
             
        } finally {
            httpclient.close();
        }
    }

}
