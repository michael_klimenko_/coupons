/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.michael.coupons.utils;

import com.typesafe.config.Config;

/**
 *
 * @author michael
 */
public class Column  {
    
    private final int id;
    private final String name;
    private final String type;
    
    public Column(final Config params) {
        id = params.getInt("id");
        name = params.getString("name");
        type = params.getString("type");
    }
    // getters, hashCode, equals, etc.

   

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

}
