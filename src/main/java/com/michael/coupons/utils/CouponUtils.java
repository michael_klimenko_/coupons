/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.michael.coupons.utils;

import com.michael.coupons.beans.Coupon;
import java.time.Instant;

/**
 *
 * @author michael
 */
public class CouponUtils {
    
  // =============== HELPERS ===================
    
    public static boolean isExpired(Coupon coupon) {
    
         return coupon.getEndDate().before(java.util.Date.from(Instant.now()));
                 
    }
    
    public static boolean isInStock(Coupon coupon) {
    
        return coupon.getAmount()>0;
    }   
}
