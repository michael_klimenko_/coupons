/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.michael.coupons.utils;

import com.michael.coupons.beans.Coupon;
import com.michael.coupons.dao.CompanyDBDAO;
import com.michael.coupons.dao.CouponDBDAO;
import com.michael.coupons.dao.CustomerDBDAO;
import com.michael.coupons.exceptions.ApplicationException;
import static com.michael.coupons.utils.ApplicationUtilities.CONFIG;
import static com.michael.coupons.utils.ApplicationUtilities.log;
import static java.lang.Thread.sleep;
import java.time.Instant;
import java.util.Calendar;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import static com.michael.coupons.utils.ApplicationUtilities.log;
import static java.lang.Thread.sleep;

/**
 * This Class is initialized at CouponSystem startup and used for removing
 * expired coupons.
 *
 * @author michael
 */
public class DailyCouponExpiration extends Thread {

    // =============== PROPERTIES ===================
    private final CompanyDBDAO companyDBDAO = CompanyDBDAO.getInstance();
    private final CustomerDBDAO customerDBDAO = CustomerDBDAO.getInstance();
    private final CouponDBDAO couponDBDAO = CouponDBDAO.getInstance();

    // interrupt flag
    private volatile boolean quit = false;

    @Override
    public void run() {
        try {

            do {
                if (!quit) {

                    //get sql data format
                    Calendar calendar = Calendar.getInstance();
                    java.sql.Date sqlJavaDateObject = new java.sql.Date(calendar.getTime().getTime());
                    log(sqlJavaDateObject + ": DailyCouponExpirationTask is started.");
                    Integer expirationInterval = CONFIG.getInt("app.coupon.expiration_interval");
                    
                    Collection<Coupon> expiredCoupons = couponDBDAO.getExpiredCoupons(sqlJavaDateObject);
                    if (!expiredCoupons.isEmpty()) {
                        for (Coupon coupon : expiredCoupons) {
                            log("Removing: " + coupon.toString());
                            //remove customer->coupon reference
                            customerDBDAO.removeCoupon(coupon.getId());
                            //remove company->coupon reference
                            companyDBDAO.removeCoupon(coupon.getId());
                            //remove coupon itsself
                            couponDBDAO.removeCoupon(coupon.getId());
                        }
                    } else {
                        log(sqlJavaDateObject + ": DailyCouponExpirationTask is ended.");
                    }

        
                    Thread.sleep(expirationInterval);
                    //Thread.sleep(86400000); //86400000 = day in millis
                    //Thread.sleep(10000); //10s
                    
                } else {
                    log("DailyCouponExpirationTask is stopped. Exiting ...");
                    return;
                }

            } while (true);

        } catch (ApplicationException ex) {
            throw new ApplicationException(ex);
        } catch (InterruptedException ex) {
            throw new ApplicationException(Exceptions.COUPON, ex);
        } finally {

        }
    }

    public void stopTask() {

        quit = true;
    }

}
