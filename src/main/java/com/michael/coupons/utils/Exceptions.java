
package com.michael.coupons.utils;

public enum Exceptions {
     NULLPOINTER("Null Pointer Exception")
    ,CONNECTION("Connection to DB is not available")
    ,SQL("SQL syntax error")
    ,COMPANY("Company management")
    ,CUSTOMER("Customer management")
    ,ACCOUNT("JSONRPC Exception")
    ,COUPON("Coupon management")
    ,UNKNOWN("Unknown error")
    ,OBJECT_DOESNT_EXIST("Object doesn't exist")
    ,INVALID_LOGIN_TYPE("Invalid clientType at login")
    ,DATABASE("Database")
    ,UNAUTHORIZED("Access is unauthorized");

    private final String value;

    private Exceptions(String msg) {
        this.value = msg;
    }
    /*
    @Override
    public String toString() {
        switch (this) {
            case NULLPOINTER:
                System.out.println("NULLPOINTER: " + value);
                break;
            case CONNECTION:
                System.out.println("Nickle: " + value);
                break;
            case SQL:
                System.out.println("Dime: " + value);
                break;
            case COMPANY_EXISTS:
                System.out.println("Company exists: " + value);
                break;
            case CUSTOMER_EXISTS:
                System.out.println("Company exists: " + value);
                break;
            case OBJECT_DOESNT_EXIST:
                System.out.println("Not found :" + value);
                break;
            case UNKNOWN:
                System.out.println("Unknown: " + value);
        }
        return super.toString();
    }
    */
}
