package com.michael.coupons.utils;

import static com.michael.coupons.utils.ApplicationUtilities.log;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Initial database mockups for testing
 *
 * @author michael
 */
public class InitDB {

    private static String dbDriver = "";  //"org.apache.derby.jdbc.ClientDriver"; //"org.apache.derby.jdbc.EmbeddedDriver";
    private static String dbURL = "";       //jdbc:derby://localhost:1527/CouponDB"; //;create=true";//user=bank;password=pass";
    private static Connection conn = null;
    private static Statement stmt = null;

    private static String tables[] = {"coupons", "companies", "customers", "customer_coupons", "company_coupons"};

    private void createConnection() {
        
        try {

//            Properties props = new Properties();
//            props.setProperty(Context.INITIAL_CONTEXT_FACTORY, this.driverName);
//            props.setProperty(Context.PROVIDER_URL, dbURL);
//            Context ctx = new InitialContext(props);
//            DataSource ds = (DataSource)ctx.lookup("jdbc/BANKDB");
//            Connection conn = ds.getConnection();
            if (!"".equals(dbDriver)) {
                Class.forName(this.dbDriver);
            }
            //   DriverManager.registerDriver(new org.apache.derby.jdbc.ClientDriver());
            //DriverManager.registerDriver(new org.apache.derby.jdbc.EmbeddedDriver());
            //Get a connection
            //conn = DriverManager.getConnection("jdbc:derby://localhost:1527/"+dbURL+";create=true"); 
            conn = DriverManager.getConnection(this.dbURL);

             log("DriverName: " + conn.getMetaData().getDriverName() + " " + conn.getMetaData().getDriverVersion(), Level.INFO, this.getClass().getName());
             log("DatabaseProductName: " + conn.getMetaData().getDatabaseProductName() + " " + conn.getMetaData().getDatabaseProductVersion(), Level.INFO, this.getClass().getName());
             log("DatabaseURL: " + conn.getMetaData().getURL(), Level.INFO, this.getClass().getName());
           

        } catch (SQLException ex) {
             log(ex.getMessage(), Level.SEVERE, this.getClass().getName());
        } catch (ClassNotFoundException ex) {
             log(ex.getMessage(), Level.SEVERE, this.getClass().getName());
        }
    }

    public void shutdown() {
         
        try {
            if (stmt != null) {
                stmt.close();
            }
            if (conn != null) {
                DriverManager.getConnection("jdbc:derby://localhost:1527/" + dbURL + ";shutdown=true");
                conn.close();
            }
        } catch (SQLException ex) {
            log(ex.getMessage(), Level.SEVERE, this.getClass().getName());
        }

    }

    public InitDB(String dbURL) {

        this.dbURL = dbURL;

        createConnection();
    }

    public InitDB(String dbURL, String dbDriver) {

        this.dbURL = dbURL;
        this.dbDriver = dbDriver;
        createConnection();
    }

    public void truncateTables() {
         
        try {
            stmt = conn.createStatement();

            for (String table : tables) {
                stmt.executeUpdate("TRUNCATE TABLE " + table);
            }
            stmt.close();
        } catch (SQLException ex) {
             log(ex.getMessage(), Level.SEVERE, this.getClass().getName());
        }
    }

    public void dropTables() {
       
        try {
            stmt = conn.createStatement();

            DatabaseMetaData databaseMetadata = conn.getMetaData();
            ResultSet resultSet;

            for (String table : tables) {
                resultSet = databaseMetadata.getTables(null, null, table.toUpperCase(), null);
                if (resultSet.next()) {
                    
                    log(table + " TABLE ALREADY EXISTS, DELETING ...");
                    stmt.executeUpdate("DROP TABLE " + table);
                } else {
                    log(table + " TABLE DOESN'T EXIST");
                }
            }
            stmt.close();
        } catch (SQLException ex) {
            
            log(ex.getMessage(), Level.SEVERE, this.getClass().getName());
        }
    }

    public void createTables() {
         
        try {
            stmt = conn.createStatement();

            if (conn.getMetaData().getURL().contains("mysql")) {
                
                log("createTables() - using mysql syntax", Level.INFO, this.getClass().getName());

                stmt.executeUpdate("CREATE TABLE companies (id BIGINT not null AUTO_INCREMENT, comp_name VARCHAR(255), password VARCHAR(16),email VARCHAR(255), PRIMARY KEY (id))");
                stmt.executeUpdate("CREATE TABLE customers (id BIGINT not null AUTO_INCREMENT, cust_name VARCHAR(255), password VARCHAR(16), PRIMARY KEY (id))");
                stmt.executeUpdate("CREATE TABLE coupons   (id BIGINT not null AUTO_INCREMENT, title VARCHAR(25),startDate DATE,endDate DATE,amount INT,type INT,message VARCHAR(255),price DOUBLE,image VARCHAR(255), PRIMARY KEY (id))");

                stmt.executeUpdate("CREATE TABLE customer_coupons (cust_id BIGINT,coupon_id BIGINT)");
                stmt.executeUpdate("CREATE TABLE company_coupons (comp_id BIGINT,coupon_id BIGINT)");

            } else if (conn.getMetaData().getURL().contains("derby")) {
                
                log("createTables() - using derby syntax", Level.INFO, this.getClass().getName());
                
                stmt.executeUpdate("CREATE TABLE companies (id BIGINT not null primary key GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), comp_name VARCHAR(255), password VARCHAR(16),email VARCHAR(255))");
                stmt.executeUpdate("CREATE TABLE customers (id BIGINT not null primary key GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), cust_name VARCHAR(255), password VARCHAR(16))");
                stmt.executeUpdate("CREATE TABLE coupons   (id BIGINT not null primary key GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), title VARCHAR(25),startDate DATE,endDate DATE,amount INT,type INT,message VARCHAR(255),price DOUBLE,image VARCHAR(255))");

                stmt.executeUpdate("CREATE TABLE customer_coupons (cust_id BIGINT,coupon_id BIGINT)");
                stmt.executeUpdate("CREATE TABLE company_coupons (comp_id BIGINT,coupon_id BIGINT)");
            }

            stmt.close();
        } catch (SQLException ex) {
            log(ex.getMessage(), Level.SEVERE, this.getClass().getName());
        }
    }

    public void createTables4Tests() {
        
        try {
            stmt = conn.createStatement();
            stmt.executeUpdate("CREATE TABLE companies (id BIGINT, comp_name VARCHAR(255), password VARCHAR(16),email VARCHAR(255))");
            stmt.executeUpdate("CREATE TABLE customers (id BIGINT, cust_name VARCHAR(255), password VARCHAR(16))");
            stmt.executeUpdate("CREATE TABLE coupons   (id BIGINT, title VARCHAR(25),startDate DATE,endDate DATE,amount INT,type INT,message VARCHAR(255),price DOUBLE,image VARCHAR(255))");

            stmt.executeUpdate("CREATE TABLE customer_coupons (cust_id BIGINT,coupon_id BIGINT)");
            stmt.executeUpdate("CREATE TABLE company_coupons (comp_id BIGINT,coupon_id BIGINT)");

            stmt.close();
        } catch (SQLException ex) {
            log(ex.getMessage(), Level.SEVERE, this.getClass().getName());
        }
    }

    public void insertRows() {
       
        try {
            stmt = conn.createStatement();

            stmt.executeUpdate("insert into customers (cust_name,password) values  ('John','1234')");
            stmt.executeUpdate("insert into customers (cust_name,password) values ('Michael','1234')");
            stmt.executeUpdate("insert into customers (cust_name,password) values ('Foo','1234')");

            stmt.executeUpdate("insert into companies (comp_name,password,email) values ('Golan','1234','golan@golan.co.il')");
            stmt.executeUpdate("insert into companies (comp_name,password,email) values ('Hot','1234','hot@hot.co.il')");
            stmt.executeUpdate("insert into companies (comp_name,password,email) values ('Badran','1234','badran@concert.co.il')");

            stmt.executeUpdate("insert into coupons (title ,startDate ,endDate ,amount ,type ,message ,price ,image ) values ('Promo1' ,'2016-05-17' ,'2017-04-21' ,100 ,7 ,'1+1 ticket', 300 ,'assets/images/angular.png')");
            stmt.executeUpdate("insert into coupons (title ,startDate ,endDate ,amount ,type ,message ,price ,image ) values ('Promo2' ,'2016-05-18' ,'2017-04-21' ,50 ,4 ,'First 50 items discount', 100 ,'assets/images/promo-icon-2.png')");
            stmt.executeUpdate("insert into coupons (title ,startDate ,endDate ,amount ,type ,message ,price ,image ) values ('Promo3' ,'2016-05-19' ,'2017-04-20' ,1000 ,3 ,'1+1 ticket', 20 ,'assets/images/promo.png')");
            stmt.executeUpdate("insert into coupons (title ,startDate ,endDate ,amount ,type ,message ,price ,image ) values ('Promo4' ,'2016-05-20' ,'2017-07-24' ,10 ,1 ,'1+1 ticket', 250 ,'assets/images/promo2.png')");
            stmt.executeUpdate("insert into coupons (title ,startDate ,endDate ,amount ,type ,message ,price ,image ) values ('Promo5' ,'2016-05-21' ,'3016-05-23' ,5 ,2 ,'Special price', 50 ,'assets/images/twitter-follow-promo-icone-4030-256.png')");

            stmt.executeUpdate("insert into company_coupons (comp_id,coupon_id) values (1,2)");
            stmt.executeUpdate("insert into company_coupons (comp_id,coupon_id) values (1,3)");
            stmt.executeUpdate("insert into company_coupons (comp_id,coupon_id) values (1,4)");
            stmt.executeUpdate("insert into company_coupons (comp_id,coupon_id) values (2,1)");
            stmt.executeUpdate("insert into company_coupons (comp_id,coupon_id) values (3,5)");

            stmt.executeUpdate("insert into customer_coupons (cust_id,coupon_id) values (1,1)");
            stmt.executeUpdate("insert into customer_coupons (cust_id,coupon_id) values (1,4)");
            stmt.executeUpdate("insert into customer_coupons (cust_id,coupon_id) values (2,3)");

            stmt.close();
        } catch (SQLException ex) {
            log(ex.getMessage(), Level.SEVERE, this.getClass().getName());
        }

    }

    public void insertRows4Tests() {
        
        try {
            stmt = conn.createStatement();

            stmt.execute("insert into companies (id,comp_name,password,email) values (10,'Golan','1234','golan@golan.co.il')");
            stmt.execute("insert into companies (id,comp_name,password,email) values (20,'Hot','1234','hot@hot.co.il')");
            stmt.execute("insert into companies (id,comp_name,password,email) values (30,'Badran','1234','badran@concert.co.il')");

            stmt.execute("insert into customers (id,cust_name,password) values (1000,'John','1234')");
            stmt.execute("insert into customers (id,cust_name,password) values (1001,'Michael','1234')");
            stmt.execute("insert into customers (id,cust_name,password) values (1002,'Foo','1234')");

            stmt.executeUpdate("insert into coupons (id , title ,startDate ,endDate ,amount ,type ,message ,price ,image ) values (7001 , 'Promo7001' ,'2016-04-17' ,'2016-04-21' ,100 ,7 ,'1+1 ticket', 300 ,'promo.jpg')");
            stmt.executeUpdate("insert into coupons (id , title ,startDate ,endDate ,amount ,type ,message ,price ,image ) values (7002 , 'Promo7002' ,'2016-04-18' ,'2016-04-21' ,50 ,4 ,'First 50 items discount', 100 ,'promo.jpg')");
            stmt.executeUpdate("insert into coupons (id , title ,startDate ,endDate ,amount ,type ,message ,price ,image ) values (7003 , 'Promo7003' ,'2016-04-19' ,'2016-04-20' ,1000 ,3 ,'1+1 ticket', 20 ,'promo.jpg')");
            stmt.executeUpdate("insert into coupons (id , title ,startDate ,endDate ,amount ,type ,message ,price ,image ) values (7004 , 'Promo7004' ,'2016-04-20' ,'2016-04-24' ,10 ,1 ,'1+1 ticket', 250 ,'promo.jpg')");
            stmt.executeUpdate("insert into coupons (id , title ,startDate ,endDate ,amount ,type ,message ,price ,image ) values (7005 , 'Promo7005' ,'2016-04-21' ,'3016-05-23' ,5 ,2 ,'Special price', 50 ,'promo.jpg')");

            stmt.execute("insert into customer_coupons (cust_id,coupon_id) values (1001,7001)");
            stmt.execute("insert into customer_coupons (cust_id,coupon_id) values (1001,7004)");
            stmt.execute("insert into customer_coupons (cust_id,coupon_id) values (1002,7003)");

            stmt.execute("insert into company_coupons (comp_id,coupon_id) values (10,7002)");
            stmt.execute("insert into company_coupons (comp_id,coupon_id) values (10,7003)");
            stmt.execute("insert into company_coupons (comp_id,coupon_id) values (10,7004)");
            stmt.execute("insert into company_coupons (comp_id,coupon_id) values (20,7001)");
            stmt.execute("insert into company_coupons (comp_id,coupon_id) values (30,7005)");

            stmt.close();
        } catch (SQLException ex) {
            log(ex.getMessage(), Level.SEVERE, this.getClass().getName());
        }

    }

    public void selectAll(String tableName) {
        try {
            stmt = conn.createStatement();
            System.out.println("\n-------------------------------------------------");

            try (
                    ResultSet results = stmt.executeQuery("select * from " + tableName)) {
                ResultSetMetaData rsmd = results.getMetaData();
                int numberCols = rsmd.getColumnCount();
                for (int i = 1; i <= numberCols; i++) {
                    //print Column Names
                    System.out.print(rsmd.getColumnLabel(i) + "\t\t");
                }

                while (results.next()) {
                    System.out.print("\n");
                    for (int i = 1; i <= numberCols; i++) {
                        //print Column Names
                        System.out.print(results.getString(i) + "\t\t");
                    }

                }
                System.out.println("\n-------------------------------------------------");
            }
            stmt.close();
        } catch (SQLException ex) {
            log(ex.getMessage(), Level.SEVERE, this.getClass().getName());
        }

    }

}
