/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.michael.coupons.utils;

import com.michael.coupons.CouponSystem;
import com.michael.coupons.beans.Company;
import com.michael.coupons.beans.Customer;
import com.michael.coupons.dao.interfaces.ICompanyDAO;
import com.michael.coupons.facades.AdminFacade;
import com.michael.coupons.facades.CompanyFacade;
import com.michael.coupons.facades.CustomerFacade;
import com.michael.coupons.facades.ICouponClientFacade;
import java.util.logging.Level;
import java.util.logging.Logger;
import static com.michael.coupons.utils.ApplicationUtilities.CONFIG;
import static java.lang.Thread.sleep;

/**
 *
 * @author michael
 */
public class Tester {

    public static void main(String args[]) {

        
       
        
        String dbURL = CONFIG.getString("database.dburl");//"CouponDB";//;user=user;password=user";
        String dbDriver = CONFIG.getString("database.dbdriver"); 
        InitDB database = new InitDB(dbURL,dbDriver);
        database.dropTables();
        database.createTables();
        database.insertRows();
        
try {
        
        /*
         DBConnectionPoolManager DBCPM = DBConnectionPoolManager.getInstance();
         Connection conn = DBCPM.getConnectionFromPool();
        try {
            PreparedStatement stmt = conn.prepareStatement(CONFIG.getString("sql.coupon.all"));
            ResultSet rs = stmt.executeQuery();
            while (rs.next()){
                System.out.println(rs.getString("title"));
            }
            
            if (rs != null) rs.close();
            if (stmt != null) stmt.close();
            if (conn != null) DBCPM.returnConnectionToPool(conn);
            
            
            System.out.println(java.util.Date.from(Instant.now()));
            System.out.println(java.sql.Date.from(Instant.now()));
            
            
            Calendar calendar = Calendar.getInstance();
            java.sql.Date ourJavaDateObject = new java.sql.Date(calendar.getTime().getTime());
            System.out.println(ourJavaDateObject);
            
            DailyCouponExpirationTask dcet = new DailyCouponExpirationTask();
            dcet.run();
        */    
        /*
        Customer customer;
        
        ICouponClientFacade companyFacade;
        ICouponClientFacade clientFacade;
        ICouponClientFacade adminFacade;
        CouponSystem CS = CouponSystem.getInstance();

        
        companyFacade = (CompanyFacade)CS.login("Golan", "1234", ClientType.COMPANY, true);
        clientFacade = (CustomerFacade)CS.login("Michael", "1234", ClientType.CUSTOMER, true);
        adminFacade  = (AdminFacade)CS.login("admin","1234",ClientType.ADMIN, true);
        
        Company company1 = new Company();
        company1.setComp_name("Test1");
        company1.setEmail("xxx@xxx.com");
        company1.setPassword("1234");
        
        Company company2 = new Company();
        company2.setComp_name("Test2");
        company2.setEmail("xxx@xxx.com");
        company2.setPassword("1234");
        
        Company company3 = new Company();
        company3.setComp_name("Test3");
        company3.setEmail("xxx@xxx.com");
        company3.setPassword("1234");
        
        ((AdminFacade)adminFacade).createCompany(company1);
        ((AdminFacade)adminFacade).createCompany(company2);
        ((AdminFacade)adminFacade).createCompany(company3);
        
                //companyFacade.createCoupon(new Coupon());
        //Collection<Coupon> coupons = clientFacade.getAllPurchasedCoupons(1001);
        
        //clientFacade.purchaseCoupon(adminFacade.getCustomer(1001), (Coupon)coupons.toArray()[0]);
        
        
        System.out.println("");
        System.out.println("======================");
        
        sleep(50000);
        
        CS.shutdown();
        
        */

        //Company myCompany = new Company((long) 200, "myCompany200", "1234", "my@com.com");
        //CompanyFacade companyFacade = new CompanyFacade();
        //AdminFacade adminFacade = new AdminFacade();

        //CompanyDBDAO myCompanyDAO = new CompanyDBDAO();
        
            //Company company;
            //company = companyFacade.retreiveCompany(200);
            //System.out.println(company.toString());
            //company = companyFacade.createCompany(myCompany);
            //company.setEmail("xxx@yyy.com");
            //company = companyFacade.updateCompany(company);
            //company = companyFacade.deleteCompany(myCompany);
            //company = companyFacade.retreiveCompany(200);
            //System.out.println(company.toString());
            /*
        for (Company company : companyFacade.retreiveAllCompanies()){
        System.out.println("*****************COMPANY*******************************************");
            System.out.println(company);
            System.out.println("|------------COPONS------------------------|");
            System.out.println(company.getCoupons());
            System.out.println("|------------------------------------|");
        }
             */
            
            
            //String string_number = "972506062002";
            //long long_number = Long.valueOf(string_number);

            //System.out.println("String number : " + string_number + "\n Long Number : "+long_number);
           // System.out.println(companyFacade.login("Hot", "1234", "company"));
            //System.out.println(adminFacade.removeCompany(companyFacade.getCompany((long) 10)).toString());
            //System.out.println("com.michael.coupons.utils.Tester.main() : " + adminFacade.getCompany(992L) );
        } catch (Exception ex) {
            Logger.getLogger(Tester.class.getName()).log(Level.SEVERE, "INFO", ex);
        } finally {
            //database.dropTables();
        }

    }
}
