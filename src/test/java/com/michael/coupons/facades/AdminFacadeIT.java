package com.michael.coupons.facades;

import com.michael.coupon.web.utils.cgrates.Account;
import com.michael.coupons.beans.Company;
import com.michael.coupons.beans.Coupon;
import com.michael.coupons.beans.Customer;
import com.michael.coupons.exceptions.ApplicationException;
import static com.michael.coupons.utils.ApplicationUtilities.log;
import com.michael.coupons.utils.InitDB;
import java.util.ArrayList;

import java.util.Collection;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import static com.michael.coupons.utils.ApplicationUtilities.CONFIG;
import static com.michael.coupons.utils.ApplicationUtilities.log;
import com.michael.coupons.utils.ClientType;

public class AdminFacadeIT {

    private static final String DBURL = CONFIG.getString("database.dburl");
    private static final InitDB DATABASE = new InitDB(DBURL);
    public static AdminFacade adminFacade = AdminFacade.getInstance();


    //Test Company variables
    public static Company test_CreateCompany;
    public static Company test_CreateExistingCompany;

    //Test Customer variables
    public static Customer test_CreateCustomer;
    public static Customer test_CreateExistingCustomer;

    @BeforeClass
    public static void setUpClass() throws Exception {

        DATABASE.dropTables();
        DATABASE.createTables4Tests();
        
        test_CreateCompany = new Company(991L, "Apple", "1234", "mail@apple.com");
        test_CreateExistingCompany = new Company(992L, "Golan", "1234", "mail@golan.com");

        test_CreateCustomer = new Customer(445L, "Avi", "6789");
        test_CreateExistingCustomer = new Customer(992L, "Michael", "6789");
 
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
        //DATABASE.dropTables();
        //DATABASE.shutdown();
    }

    @Before
    public void setUp() throws Exception {
        DATABASE.insertRows4Tests();
    }

    @After
    public void tearDown() throws Exception {
        DATABASE.truncateTables();
    }
    
    
    public AdminFacadeIT() {
    }

    /**
     * Test of createCompany method, of class AdminFacade.
     *
     * @throws java.lang.Exception
     */
    //@Test
    public void testCreateCompany() throws Exception {
        Company company = test_CreateCompany;
        log("createCompany: " + company.toString());

        Company expResult = company;
        Company result = adminFacade.createCompany(company);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of createCompany method, of class AdminFacade. should throw an
     * exception of existing company
     *
     * @throws java.lang.Exception
     */
    //@Test(expected = ApplicationException.class)
    public void testCreateExistingCompany() throws Exception {

        Company company = test_CreateExistingCompany;
        log("createExistingCompany: " + company.toString());

        Company expResult = company;
        Company result = adminFacade.createCompany(company);
        assertEquals(expResult, result);
    }

    /**
     * Test of removeCompany method, of class AdminFacade.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testRemoveCompany() throws Exception {
        
        Company company = adminFacade.getCompany(10L);
        
        log("removeCompany: " + company.toString());

        int expResultRemovedCompany = 1;
        Object expResultCustomerCoupons = null;
        Object expResultCompanyCoupons = null;
        
        Collection<Coupon> companyCoupons = company.getCoupons();
        
        int resultRemovedCompany = adminFacade.removeCompany(company.getId());

        assertEquals(expResultRemovedCompany, resultRemovedCompany);

        if (companyCoupons != null) {
            for (Coupon coupon : companyCoupons) {
                
                //assumes that there will be no customerID in customer_coupons table  
                assertEquals(expResultCustomerCoupons, adminFacade.getCustomersByCouponId(coupon.getId()));
                
                //assumes that there will be no companyID in company_coupons table
                assertEquals(expResultCompanyCoupons, adminFacade.getCompanyIdByCouponId(coupon.getId()));
            }
            
        }

    }

    /**
     * Test of updateCompany method, of class AdminFacade.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testUpdateCompany() throws Exception {

        Company company = adminFacade.getCompany(20L);

        log("updateCompany: " + company);

        company.setPassword("9898");
        company.setEmail("xxx@yyy.com");

        Company update = adminFacade.updateCompany(company);

        String expResultPassword = "9898";
        String expResultEmail = "xxx@yyy.com";

        Company result = adminFacade.getCompany(company.getId());

        assertEquals(expResultPassword, result.getPassword());
        assertEquals(expResultEmail, result.getEmail());
    }

    /**
     * Test of getCompany method, of class AdminFacade.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetCompany() throws Exception {

        Long id = 20L;
        log("getCompany: " + id);

        String expResult = "Hot";
        Company result = adminFacade.getCompany(id);
        assertEquals(expResult, result.getComp_name());

    }

    /**
     * Test of getAllCompanies method, of class AdminFacade.
     * @throws java.lang.Exception
     */
    @Test
    public void testGetAllCompanies() throws Exception {
        

        List<String> expResult = new ArrayList() {
            {
                add("Golan");
                add("Hot");
                add("Badran");
            }
        };
        log("getAllCompanies: "+expResult.toString());
        
        Collection<Company> companies = adminFacade.getAllCompanies();
        List<String> result = new ArrayList();
        companies.stream().forEach((_item) -> {

            result.add(_item.getComp_name());
            log("got: "+_item.getComp_name());
        });

        assertEquals(true, expResult.containsAll(result));

    }

    /**
     * Test of createCustomer method, of class AdminFacade.
     *
     * @throws java.lang.Exception
     */
    //@Test
    public void testCreateCustomer() throws Exception {
        Customer customer = test_CreateCustomer;
        log("createCustomer: " + customer.toString());

        Customer expResult = customer;
        Customer result = adminFacade.createCustomer(customer);
        assertEquals(expResult, result);
    }

    /**
     * Test of createCustomer method, of class AdminFacade.
     *
     * @throws java.lang.Exception
     */
    //@Test(expected = ApplicationException.class)
    public void testCreateExistingCustomer() throws Exception {
        Customer customer = test_CreateExistingCustomer;
        log("createExistingCustomer: " + customer.toString());

        Customer expResult = null;
        Customer result = adminFacade.createCustomer(customer);
        assertEquals(expResult, result);
    }

    /**
     * Test of removeCustomer method, of class AdminFacade.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testRemoveCustomer() throws Exception {
        Customer customer = adminFacade.getCustomer(1001L);
        
        log("removeCustomer: " + customer.toString());

        int expResultCustomer = 1;
        
        Object expResultCoupons = null;
        
        //Collect coupons of the customer        
        Collection<Coupon> customerCoupons = customer.getCoupons();
        
        Object result = adminFacade.removeCustomer(customer.getId());

        assertEquals(expResultCustomer, result);

        for (Coupon coupon : customerCoupons) {
            //assumes that there will be no customerID in customer_coupons table 
            assertEquals(expResultCoupons, adminFacade.getCustomersByCouponId(coupon.getId()));
        }
    }

    /**
     * Test of updateCustomer method, of class AdminFacade.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testUpdateCustomer() throws Exception {

        Customer customer = adminFacade.getCustomer(1002L);
        log("updateCustomer: " + customer.toString());
        customer.setPassword("3333");

        String expResultPassword = "3333";
        adminFacade.updateCustomer(customer);
        String result = adminFacade.getCustomer(customer.getId()).getPassword();
        log("result: " + result);
        assertEquals(expResultPassword, result);
    }

    /**
     * Test of getCustomer method, of class AdminFacade.
     * @throws java.lang.Exception
     */
    @Test
    public void testGetCustomer() throws Exception {

        Long id = 1001L;
        log("getCustomer: " + id);

        String expResult = "Michael";
        Customer result = adminFacade.getCustomer(id);
        assertEquals(expResult, result.getCust_name());

    }

    /**
     * Test of retreiveAllCustomers method, of class AdminFacade.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testRetreiveAllCustomers() throws Exception {
        

        List<String> expResult = new ArrayList() {
            {
                add("Michael");
                add("Foo");
                add("John");
            }
        };
        
        log("getAllCustomers: " + expResult);
        
        Collection<Customer> customers = adminFacade.getAllCustomers();
        List<String> result = new ArrayList();
        customers.stream().forEach((_item) -> {

            result.add(_item.getCust_name());
        });
        log("result: " + result);
        assertEquals(true, expResult.containsAll(result));

    }

    /**
     * Test of login method, of class AdminFacade.
     */
    @Test
    public void testLogin() throws Exception {

        // Admin login
        String username = "admin";
        String password = "1234";
        ClientType clientType = ClientType.ADMIN;
        log("login : (" + clientType + ") " + username + "@" + password);

        boolean expResult = true;
        boolean result = adminFacade.login(username, password, clientType);
        assertEquals(expResult, result);

        //Wrong login
        username = "Admin";
        password = "1234";
        clientType = null;
        log("login : (" + clientType + ") " + username + "@" + password);

        expResult = false;
        result = adminFacade.login(username, password, clientType);
        assertEquals(expResult, result);

    }
    
    @Test
    public void testGetAccount() throws Exception {

        String msisdn = "972585577773";
        log("getAccount: " + msisdn);

        String expResult = "golant:972585577773";
        Account result = adminFacade.getAccount(msisdn);
        assertEquals(expResult, result.getID());

    }
    
    @Test
    public void testGetAccounts() throws Exception {

        int expResult = 1;
        Account[] result = adminFacade.getAccounts();
        for (Account acc : result) {
            log("getAccounts: got " + acc.getID());
        }
        assertEquals(expResult, result.length);

    }

}
