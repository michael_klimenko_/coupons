package com.michael.coupons.facades;

import static com.michael.coupons.utils.ApplicationUtilities.log;
import static com.michael.coupons.utils.ApplicationUtilities.getFormatedDate;
import com.michael.coupons.beans.Company;
import com.michael.coupons.beans.Coupon;
import com.michael.coupons.entities.Category;
import static com.michael.coupons.facades.AdminFacadeIT.adminFacade;
import static com.michael.coupons.utils.ApplicationUtilities.log;
import com.michael.coupons.utils.InitDB;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static com.michael.coupons.utils.ApplicationUtilities.CONFIG;
import static com.michael.coupons.utils.ApplicationUtilities.log;
import static com.michael.coupons.utils.ApplicationUtilities.log;
import com.michael.coupons.utils.ClientType;

/**
 *
 * @author michael
 */
public class CompanyFacadeIT {

    private static final String DBURL = CONFIG.getString("database.dburl"); //"CouponDB";//;user=user;password=user";
    private static final InitDB DATABASE = new InitDB(DBURL);
    public static CustomerFacade customerFacade = CustomerFacade.getInstance();
    public static AdminFacade adminFacade = AdminFacade.getInstance();
    public static CompanyFacade companyFacade = CompanyFacade.getInstance();

    //Test Company variables
    public static Company test_CreateCompany;
    public static Company test_CreateExistingCompany;

    //Test Coupon variables
    public static java.sql.Date currentSQLTime;
    public static Coupon test_CreateCoupon;
    public static Coupon test_CreateExistingCoupon;

    public CompanyFacadeIT() {
    }

    @BeforeClass
    public static void setUpClass() {

        DATABASE.dropTables();
        DATABASE.createTables4Tests();

        test_CreateCompany = new Company(991L, "Apple", "1234", "mail@apple.com");
        test_CreateExistingCompany = new Company(992L, "Golan", "1234", "mail@golan.com");

        currentSQLTime = new java.sql.Date(Calendar.getInstance().getTime().getTime());

        test_CreateCoupon = new Coupon(7010L, "Test Coupon", currentSQLTime, currentSQLTime, 100, Category.SPORTS, "This is a test coupon", 100.0, "image.jpg");
        test_CreateExistingCoupon = new Coupon(7010L, "Promo", currentSQLTime, currentSQLTime, 100, Category.SPORTS, "This is a test coupon", 100.0, "image.jpg");

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        DATABASE.insertRows4Tests();
    }

    @After
    public void tearDown() {
        DATABASE.truncateTables();
    }

    /**
     * Test of createCoupon method, of class CompanyFacade.
     */
    //@Test
    public void testCreateCoupon() {
        Coupon coupon = test_CreateCoupon;
        Long comp_id = 10L;
        log("createCoupon: " + coupon.toString());

        Coupon expResult = coupon;
        Coupon result = companyFacade.createCoupon(comp_id, coupon);
        assertEquals(expResult, result);

    }

    /**
     * Test of removeCoupon method, of class CompanyFacade.
     */
    @Test
    public void testRemoveCoupon() {

        Long coupon_id = 7005L;
        Long comp_id = 30L;
        
        log("removeCoupon: id: " + coupon_id);

        int expResult = 1;
        int result = companyFacade.removeCoupon(comp_id,coupon_id);
        assertEquals(expResult, result);

    }

    /**
     * Test of updateCoupon method, of class CompanyFacade.
     */
    @Test
    public void testUpdateCoupon() {
        Coupon coupon = companyFacade.getCoupon(7004L);
        log("updateCoupon: " + coupon);

        String message = "Updated message";
        coupon.setMessage(message);

        String expResult = message;
        companyFacade.updateCoupon(10L,coupon);

        String result = companyFacade.getCoupon( coupon.getId()).getMessage();

        log("result: " + result);

        assertEquals(expResult, result);

    }

    /**
     * Test of getCoupon method, of class CompanyFacade.
     */
    @Test
    public void testGetCoupon() {

        Long id = 7004L;

        log("getCoupon: id: " + id);

        Long expResult = id;
        Long result = companyFacade.getCoupon(id).getId();
        assertEquals(expResult, result);

    }

    /**
     * Test of getAllCoupons method, of class CompanyFacade.
     */
    @Test
    public void testGetAllCoupons() {

        Long id = 10L;

        log("getAllCoupons: id: " + id);

        //ids of coupons that should be returned
        List<Long> expResult = new ArrayList() {
            {
                add(7002L);
                add(7003L);
                add(7004L);
            }
        };

        Collection<Coupon> coupons = companyFacade.getAllCoupons(id);

        List<Long> result = new ArrayList();
        coupons.stream().forEach((coupon) -> {
            result.add(coupon.getId());
            log("got: " + coupon.toString());
        });
        assertEquals(true, expResult.containsAll(result));

    }

    /**
     * Test of getCouponByType method, of class CompanyFacade.
     */
    @Test
    public void testGetCouponByType() {

        Long id = 10L;
        Category type = Category.ENTERTAINMENT;

        log("getCouponByType: id: " + id + ", type: " + type.name());

        //ids of coupons that should be returned
        List<Long> expResult = new ArrayList() {
            {
                add(7001L);
            }
        };

        Collection<Coupon> coupons = companyFacade.getCouponByType(id, type);

        List<Long> result = new ArrayList();
        coupons.stream().forEach((coupon) -> {
            result.add(coupon.getId());
            log("got: " + coupon.toString());
        });
        assertEquals(true, expResult.containsAll(result));

    }

    /**
     * Test of getCouponByPrice method, of class CompanyFacade.
     */
    @Test
    public void testGetCouponByPrice() {

        Long id = 10L;
        double price = 100.0;

        log("getCouponByPrice: id: " + id + ", price: " + price);

        //ids of coupons that should be returned
        List<Long> expResult = new ArrayList() {
            {
                add(7002L);
                add(7003L);
            }
        };

        Collection<Coupon> coupons = companyFacade.getCouponByPrice(id, price);

        List<Long> result = new ArrayList();
        coupons.stream().forEach((coupon) -> {
            result.add(coupon.getId());
            log("got: " + coupon.toString());
        });
        assertEquals(true, expResult.containsAll(result));

    }

    /**
     * Test of getCouponByDate method, of class CompanyFacade.
     *
     * @throws java.text.ParseException
     */
    @Test
    public void testGetCouponByDate() throws ParseException {

        Long id = 10L;
       
        String date = "2016-04-19";
        
//        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
//        java.util.Date parsed = format.parse("2016-04-19");
//        java.sql.Date date = new java.sql.Date(parsed.getTime());

        log("getCouponByDate: id: " + id + ", date: " + date);

        //ids of coupons that should be returned
        List<Long> expResult = new ArrayList() {
            {
                add(7002L);
                add(7003L);
            }
        };

        Collection<Coupon> coupons = companyFacade.getCouponByDate(id, date);

        List<Long> result = new ArrayList();
        
        coupons.stream().forEach((coupon) -> {
            result.add(coupon.getId());
            log("got: " + coupon.toString());
        });
        
        assertEquals(true, expResult.containsAll(result));

    }

    /**
     * Test of getCompany method, of class CompanyFacade.
     */
    @Test
    public void testGetCompany() {

        Long id = 20L;
        log("getCompany: " + id);

        String expResult = "Hot";
        Company result = companyFacade.getCompany(id);
        assertEquals(expResult, result.getComp_name());

    }

    /**
     * Test of retreiveAllCompanies method, of class CompanyFacade.
     */
    @Test
    public void testRetreiveAllCompanies() {

        List<String> expResult = new ArrayList() {
            {
                add("Golan");
                add("Hot");
                add("Badran");
            }
        };
        log("getAllCompanies: " + expResult.toString());

        Collection<Company> companies = companyFacade.getAllCompanies();
        List<String> result = new ArrayList();
        companies.stream().forEach((_item) -> {

            result.add(_item.getComp_name());
            log("got: " + _item.getComp_name());
        });

        assertEquals(true, expResult.containsAll(result));

    }

    /**
     * Test of createCompany method, of class CompanyFacade.
     */
    //@Test
    public void testCreateCompany() {

        Company company = test_CreateCompany;

        log("createCompany: " + company.toString());

        Company expResult = company;
        Company result = companyFacade.createCompany(company);
        assertEquals(expResult, result);

    }

    /**
     * Test of deleteCompany method, of class CompanyFacade.
     */
    @Test
    public void testDeleteCompany() {

        Long comp_id = 10L;

        log("deleteCompany: id: " + comp_id);

        int expResult = 1;
        int result = companyFacade.deleteCompany(comp_id);

        assertEquals(expResult, result);

    }

    /**
     * Test of updateCompany method, of class CompanyFacade.
     */
    @Test
    public void testUpdateCompany() {

        Company company = adminFacade.getCompany(20L);
        log("updateCompany: " + company.toString());
                
        company.setPassword("secret");
        String expResult = "secret";
        
        companyFacade.updateCompany(company);
        String result = adminFacade.getCompany(company.getId()).getPassword();
        log("result: " + result);
        assertEquals(expResult, result);

    }

    /**
     * Test of login method, of class CompanyFacade.
     */
    @Test
    public void testLogin() {
        // Customer login
        String username = "Hot";
        String password = "1234";
        ClientType clientType = ClientType.COMPANY;
        log("login : (" + clientType + ") " + username + "@" + password);

        boolean expResult = true;
        boolean result = companyFacade.login(username, password, clientType);
        log("Result: " + result);
        assertEquals(expResult, result);

        //Wrong login
        username = "Jon";
        password = "1234";
        clientType = ClientType.COMPANY;
        log("login : (" + clientType + ") " + username + "@" + password);

        expResult = false;
        result = companyFacade.login(username, password, clientType);
        log("Result: " + result);
        assertEquals(expResult, result);

    }

}
