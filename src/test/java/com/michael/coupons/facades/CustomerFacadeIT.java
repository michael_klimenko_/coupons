/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.michael.coupons.facades;

import com.michael.coupons.beans.Coupon;
import com.michael.coupons.beans.Customer;
import com.michael.coupons.entities.Category;
import com.michael.coupons.exceptions.ApplicationException;
import com.michael.coupons.utils.InitDB;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static com.michael.coupons.utils.ApplicationUtilities.CONFIG;
import static com.michael.coupons.utils.ApplicationUtilities.log;
import com.michael.coupons.utils.ClientType;

/**
 *
 * @author michael
 */
public class CustomerFacadeIT {

    private static final String DBURL = CONFIG.getString("database.dburl");
    private static final InitDB DATABASE = new InitDB(DBURL);
    
    public static CustomerFacade customerFacade = CustomerFacade.getInstance();
    public static AdminFacade adminFacade = AdminFacade.getInstance();
    public static CompanyFacade companyFacade = CompanyFacade.getInstance();
    
    @BeforeClass
    public static void setUpClass() {

        DATABASE.dropTables();
        DATABASE.createTables4Tests();
        //DATABASE.insertRows();

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() throws Exception {
        DATABASE.insertRows4Tests();
    }

    @After
    public void tearDown() throws Exception {
        DATABASE.truncateTables();
    }

    public CustomerFacadeIT() {
    }

    /**
     * Test of purchaseCoupon method, of class CustomerFacade.
     */
    @Test
    public void testPurchaseCoupon() {
        
        Customer customer = adminFacade.getCustomer(1001L);
        Coupon coupon = companyFacade.getCoupon(7005L);
        //Long cust_id = 1001L;
        //Long coupon_id = 7005L;
        
         log("purchaseCoupon: customer: " + customer.getCust_name() + ", coupon id: " + coupon.getId() + ", amount: " + coupon.getAmount() + ", expiration date: " + coupon.getEndDate());
        

        int expResultAmount = 4;
        
        
        Coupon couponReturned = customerFacade.purchaseCoupon(customer.getId(), coupon.getId());
        int resultAmount = companyFacade.getCoupon(coupon.getId()).getAmount();
        boolean result = adminFacade.getCustomersByCouponId(couponReturned.getId()).contains(customer.getId());
        
        log("amount: " + resultAmount);
        
        log("purchased : " + result);
        
        assertEquals(true, result);
     
        //Verify amount
        assertEquals(expResultAmount, resultAmount);
    }

     /**
     * Test of purchaseCoupon method, of class CustomerFacade.
     * case expired should throw exception
     */
    @Test(expected = ApplicationException.class)
    public void testPurchaseExpiredCoupon() {
        
        Customer customer = adminFacade.getCustomer(1001L);
        Coupon coupon = companyFacade.getCoupon(7003L);
        
         log("purchaseCoupon: customer: " + customer.getCust_name() + ", coupon id: " + coupon.getId() + ", amount: " + coupon.getAmount() + ", expiration date: " + coupon.getEndDate());
        
        customerFacade.purchaseCoupon(customer.getId(), coupon.getId());
       
    }
     
    /**
     * Test of purchaseCoupon method, of class CustomerFacade.
     * case out of stock should throw exception
     */
    @Test(expected = ApplicationException.class)
    public void testPurchaseOutOfStockCoupon() {
        
        Customer customer = adminFacade.getCustomer(1001L);
        Coupon coupon = companyFacade.getCoupon(7005L);
        coupon.setAmount(0);
        companyFacade.updateCoupon(30L, coupon);
        
         log("purchaseCoupon: customer: " + customer.getCust_name() + ", coupon id: " + coupon.getId() + ", amount: " + coupon.getAmount() + ", expiration date: " + coupon.getEndDate());
        
        customerFacade.purchaseCoupon(customer.getId(), coupon.getId());
       
    }
    
     /**
     * Test of purchaseCoupon method, of class CustomerFacade.
     * case already purchased should throw exception
     */
    @Test(expected = ApplicationException.class)
    public void testPurchaseAlreadyPurchasedCoupon() {
        
        Customer customer = adminFacade.getCustomer(1001L);
        Coupon coupon = companyFacade.getCoupon(7001L);
        
        log("purchaseCoupon: customer: " + customer.getCust_name() + ", coupon id: " + coupon.getId() + ", amount: " + coupon.getAmount() + ", expiration date: " + coupon.getEndDate());
        
        customerFacade.purchaseCoupon(customer.getId(), coupon.getId());
       
    }
    
    /**
     * Test of getAllPurchasedCoupons method, of class CustomerFacade.
     */
    @Test
    public void testGetAllPurchasedCoupons() {
        log("getAllPurchasedCoupons");
        Long id = 1001L;
        
         //ids of coupons that should be returned
        List<Long> expResult = new ArrayList() {
            {
                add(7001L);
                add(7004L);
            }
        };

        Collection<Coupon> coupons = customerFacade.getAllPurchasedCoupons(id);

        List<Long> result = new ArrayList();
        coupons.stream().forEach((coupon) -> {
            result.add(coupon.getId());
            log("got: " + coupon.toString());
        });

        assertEquals(true, expResult.containsAll(result));
        
    }

    /**
     * Test of getAllPurchasedCouponsByType method, of class CustomerFacade.
     */
    @Test
    public void testGetAllPurchasedCouponsByType() {

        Long id = 1001L;
        Category type = Category.ENTERTAINMENT;
        log("getAllPurchasedCouponsByType: customer id: " + id + ", type: " + type);

        //ids of coupons that should be returned
        List<Long> expResult = new ArrayList() {
            {
                add(7001L);
            }
        };

        Collection<Coupon> coupons = customerFacade.getAllPurchasedCouponsByType(id, type);

        List<Long> result = new ArrayList();
        coupons.stream().forEach((coupon) -> {
            result.add(coupon.getId());
            log("got: " + coupon.toString());
        });

        assertEquals(true, expResult.containsAll(result));
    }

    /**
     * Test of getAllPurchasedCouponsByPrice method, of class CustomerFacade.
     */
    @Test
    public void testGetAllPurchasedCouponsByPrice() {

        Long id = 1001L;
        double price = 250.0;
        log("getAllPurchasedCouponsByPrice: customer id: " + id + ", price: " + price);

        //ids of coupons that should be returned
        List<Long> expResult = new ArrayList() {
            {
                add(7004L);
            }
        };

        Collection<Coupon> coupons = customerFacade.getAllPurchasedCouponsByPrice(id, price);

        List<Long> result = new ArrayList();
        coupons.stream().forEach((coupon) -> {
            result.add(coupon.getId());
            log("got: " + coupon.toString());
        });

        assertEquals(true, expResult.containsAll(result));

    }

    /**
     * Test of login method, of class CustomerFacade.
     */
    @Test
    public void testLogin() {
        // Customer login
        String username = "Michael";
        String password = "1234";
        ClientType clientType = ClientType.CUSTOMER;
        log("login : (" + clientType + ") " + username + "@" + password);

        boolean expResult = true;
        boolean result = customerFacade.login(username, password, clientType);
        log("Result: " + result);
        assertEquals(expResult, result);

        //Wrong login
        username = "Jon";
        password = "1234";
        clientType = ClientType.CUSTOMER;
        log("login : (" + clientType + ") " + username + "@" + password);

        expResult = false;
        result = customerFacade.login(username, password, clientType);
        log("Result: " + result);
        assertEquals(expResult, result);

    }

}
